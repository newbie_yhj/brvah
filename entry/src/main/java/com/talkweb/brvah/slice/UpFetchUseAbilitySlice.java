package com.talkweb.brvah.slice;

import com.talkweb.brvah.ResourceTable;
import com.talkweb.brvah.adapter.UpFetchAdapter;
import com.talkweb.brvah.base.BaseSlice;
import com.talkweb.brvah.entity.Movie;
import com.talkweb.floatingbutton.util.LogUtil;
import com.talkweb.library.adapter.base.BaseQuickAdapter;
import com.talkweb.library.adapter.base.BaseViewHolder;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Component;
import ohos.agp.components.DirectionalLayoutManager;
import ohos.agp.components.ListContainer;
import ohos.agp.components.Text;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

public class UpFetchUseAbilitySlice extends BaseSlice {

    private ListContainer mRecyclerView;
    private UpFetchAdapter mAdapter;
    private int count;

    @Override
    public int getLayout() {
        return ResourceTable.Layout_ability_up_fetch_use;
    }

    @Override
    protected void initWidget() {
        setBackBtn();
        setTitle("UpFetch Use");

        mRecyclerView = (ListContainer) findComponentById(ResourceTable.Id_rv);
        mAdapter = new UpFetchAdapter(ResourceTable.Layout_item_movie, genData());
        mRecyclerView.setItemProvider(mAdapter);
        mAdapter.setUpFetchEnable(true);

        mAdapter.setStartUpFetchPosition(2);
        mAdapter.setUpFetchListener(new BaseQuickAdapter.UpFetchListener() {
            @Override
            public void onUpFetch() {
                LogUtil.error("onUpFetch", "onUpFetch" + count);
                startUpFetch();
            }
        });
    }

    @Override
    protected void initData() {

    }

    private void startUpFetch() {
        count++;
        mAdapter.setUpFetching(true);
        getUITaskDispatcher().delayDispatch(new Runnable() {
            @Override
            public void run() {
                mAdapter.addData(0, genData());
                mAdapter.setUpFetching(false);
                if (count > 3) {
                    mAdapter.setUpFetchEnable(false);
                }
            }
        }, 500);
    }

    int no = 1;
    private List<Movie> genData() {
        ArrayList<Movie> list = new ArrayList<>();
        Random random = new Random();
        for (int i = 0; i < 10; i++) {
            String name = "Chad";
            int price = no++; //random.nextInt(10) + 10;
            int len = random.nextInt(80) + 60;
            Movie movie = new Movie(name, len, price, "He was one of Australia's most distinguished artistes");
            list.add(movie);
        }
        return list;
    }
}
