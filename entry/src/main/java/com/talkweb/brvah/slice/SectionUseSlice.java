package com.talkweb.brvah.slice;

import com.talkweb.brvah.ResourceTable;
import com.talkweb.brvah.adapter.SectionAdapter;
import com.talkweb.brvah.base.BaseSlice;
import com.talkweb.brvah.data.DataServer;
import com.talkweb.brvah.entity.MySection;
import com.talkweb.brvah.util.DialogUtil;
import com.talkweb.brvah.util.LogUtils;
import com.talkweb.library.adapter.base.BaseQuickAdapter;
import ohos.agp.components.Component;
import ohos.agp.components.ListContainer;
import ohos.agp.components.TableLayoutManager;
import ohos.agp.components.element.ShapeElement;

import java.util.List;

/**
 * @author: jun.xiong
 * @date: 2021/5/10 11:02
 * @description:
 */
public class SectionUseSlice extends BaseSlice {

    private ListContainer listContainer;
    private List<MySection> mData;

    @Override
    public int getLayout() {
        return ResourceTable.Layout_ability_section_use;
    }

    @Override
    protected void initWidget() {
        setTitle("Section Use");
        setBackBtn();
        listContainer = findView(ResourceTable.Id_list_section);
        TableLayoutManager manager = new TableLayoutManager();
//        manager.setColumnCount(2);
//        manager.setRowCount(2);
        listContainer.setLayoutManager(manager);
    }

    @Override
    protected void initData() {
        mData = DataServer.getSampleData();
        SectionAdapter sectionAdapter = new SectionAdapter(ResourceTable.Layout_item_section_content, ResourceTable.Layout_def_section_head, mData);

//        sectionAdapter.setSpanSizeLookup(new BaseQuickAdapter.SpanSizeLookup() {
//            @Override
//            public int getSpanSize(TableLayoutManager gridLayoutManager, int position) {
//                MySection mySection = mData.get(position);
//                LogUtils.syso("itemclicck：" + mySection.toString());
//                if (mySection.isHeader)
//                    return 1;
//                else
//                    return 2;
//            }
//        });

        sectionAdapter.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseQuickAdapter adapter, Component view, int position) {
                MySection mySection = mData.get(position);
                LogUtils.syso("itemclicck：" + mySection.toString());
                if (mySection.isHeader)
                    DialogUtil.toastShort(getContext(), "headerClick:" + mySection.header);
                else
                    DialogUtil.toastShort(getContext(), "onItemClick:" + position + "-" + mySection.t.getName());
            }
        });
        sectionAdapter.setOnItemChildClickListener(new BaseQuickAdapter.OnItemChildClickListener() {
            @Override
            public void onItemChildClick(BaseQuickAdapter adapter, Component view, int position) {
                DialogUtil.toastShort(getContext(), "onItemChildClick:" + position);
                LogUtils.syso("itemchildclick：" + position);
            }
        });
        listContainer.setItemProvider(sectionAdapter);

    }
}
