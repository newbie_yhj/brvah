package com.talkweb.brvah.slice;

import com.talkweb.brvah.ResourceTable;
import com.talkweb.brvah.adapter.PullToRefreshAdapter;
import com.talkweb.brvah.base.BaseSlice;
import com.talkweb.brvah.data.DataServer;
import com.talkweb.brvah.entity.Status;
import com.talkweb.brvah.util.DialogUtil;
import com.talkweb.library.adapter.base.BaseQuickAdapter;
import com.talkweb.library.adapter.base.loadmore.SimpleLoadMoreView;
import com.talkweb.library.adapter.base.swipelayout.CommonRefreshHeader;
import com.talkweb.library.adapter.base.swipelayout.IRefresh;
import com.talkweb.library.adapter.base.swipelayout.SwipeRefreshContainer;
import ohos.agp.components.*;
import ohos.eventhandler.EventHandler;
import ohos.eventhandler.EventRunner;
import ohos.multimodalinput.event.TouchEvent;

import java.util.List;

/**
 * @author: jun.xiong
 * @date: 2021/5/18 15:25
 * @description:
 */
public class PullToRefreshSlice extends BaseSlice {

    private ListContainer listContainer;
    private SwipeRefreshContainer refreshContainer;
    private PullToRefreshAdapter adapter;

    private int mNextRequestPage = 1;
    private static final int PAGE_SIZE = 10;


    @Override
    public int getLayout() {
        return ResourceTable.Layout_ability_pull_refresh_use;
    }

    @Override
    protected void initWidget() {
        setBackBtn();
        setTitle("PullToRefresh Use");
        listContainer = findView(ResourceTable.Id_pull_list);
        refreshContainer = findView(ResourceTable.Id_swipe_layout);
        listContainer.setLayoutManager(new DirectionalLayoutManager());
    }

    @Override
    protected void initData() {
        adapter = new PullToRefreshAdapter();
//        adapter.setOnLoadMoreListener(new BaseQuickAdapter.RequestLoadMoreListener() {
//            @Override
//            public void onLoadMoreRequested() {
//                loadMore();
//            }
//        }, listContainer);
        adapter.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseQuickAdapter adapter, Component view, int position) {
                DialogUtil.toastShort(getContext(), "click item:" + position);
            }
        });

//        listContainer.setTouchEventListener(new Component.TouchEventListener() {
//            @Override
//            public boolean onTouchEvent(Component component, TouchEvent touchEvent) {
//                System.out.println("touchlist："+ touchEvent.getAction());
////                return refreshContainer.onTouchEvent(component, touchEvent);
//                return false;
//            }
//        });
        listContainer.setItemProvider(adapter);
        addHeadView();
        initRefreshLayout();
//        refreshContainer.setAutoLayout(true);
//        refresh();
    }

    private void addHeadView() {
        Component headView = LayoutScatter.getInstance(this).parse(ResourceTable.Layout_head_view, null, false);
        headView.findComponentById(ResourceTable.Id_iv).setVisibility(Component.HIDE);
        ((Text) headView.findComponentById(ResourceTable.Id_tv)).setText("change load view");
        headView.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                adapter.setNewData(null);
//                adapter.setLoadMoreView(new SimpleLoadMoreView());
//                listContainer.setItemProvider(adapter);

                DialogUtil.toastShort(getContext(), "change complete");
                refreshContainer.setDisableRefreshScroll(true);
                refresh();
            }
        });
        adapter.addHeaderView(headView);
    }


    private void initRefreshLayout() {
        // 下拉刷新
        CommonRefreshHeader header = new CommonRefreshHeader(this);
        refreshContainer.setRefreshOverComponent(header);
        refreshContainer.setRefreshListener(new IRefresh.RefreshListener() {
            @Override
            public void onRefresh() {
                refresh();
//                getContext().getUITaskDispatcher().delayDispatch(new Runnable() {
//                    @Override
//                    public void run() {
//                        refreshContainer.refreshFinish();
//                    }
//                }, 1000);
            }

            @Override
            public boolean enableRefresh() {
                return true;
            }
        });
    }

    private void refresh() {
        mNextRequestPage = 1;
//        adapter.setEnableLoadMore(false);//这里的作用是防止下拉刷新的时候还可以上拉加载
        new Request(mNextRequestPage, new RequestCallBack() {
            @Override
            public void success(List<Status> data) {
                setData(true, data);
//                adapter.setEnableLoadMore(true);
                refreshContainer.setDisableRefreshScroll(false);
                refreshContainer.refreshFinish();
            }

            @Override
            public void fail(Exception e) {
                DialogUtil.toastShort(getContext(), "net work error");
//                adapter.setEnableLoadMore(true);
                refreshContainer.setDisableRefreshScroll(false);
                refreshContainer.refreshFinish();
            }
        }).start();
    }

    private void loadMore() {
        new Request(mNextRequestPage, new RequestCallBack() {
            @Override
            public void success(List<Status> data) {

                boolean isRefresh = mNextRequestPage == 1;
                setData(isRefresh, data);
            }

            @Override
            public void fail(Exception e) {
                adapter.loadMoreFail();
                DialogUtil.toastShort(PullToRefreshSlice.this, "net work error");
            }
        }).start();
    }


    private void setData(boolean isRefresh, List<Status> data) {
        mNextRequestPage++;
        final int size = data == null ? 0 : data.size();
        if (isRefresh) {
            adapter.setNewData(data);
        } else {
            if (size > 0) {
                adapter.addData(data);
            }
        }
        if (size < PAGE_SIZE) {
            //第一页如果不够一页就不显示没有更多数据布局
            adapter.loadMoreEnd(isRefresh);
            DialogUtil.toastShort(this, "no more data");
        } else {
            adapter.loadMoreComplete();
        }
    }

    private interface RequestCallBack {
        void success(List<Status> data);

        void fail(Exception e);
    }

    private static class Request extends Thread {
        private final int mPage;
        private RequestCallBack mCallBack;
        private EventHandler mHandler;

        private static boolean mFirstPageNoMore;
        private static boolean mFirstError = true;

        public Request(int page, RequestCallBack callBack) {
            mPage = page;
            mCallBack = callBack;
            mHandler = new EventHandler(EventRunner.getMainEventRunner());
        }

        @Override
        public void run() {
            try {
                Thread.sleep(500);
            } catch (InterruptedException e) {
            }

            if (mPage == 2 && mFirstError) {
                mFirstError = false;
                mHandler.postTask(new Runnable() {
                    @Override
                    public void run() {
                        mCallBack.fail(new RuntimeException("fail"));
                    }
                });
            } else {
                int size = PAGE_SIZE;
                if (mPage == 1) {
                    if (mFirstPageNoMore) {
                        size = 1;
                    }
                    mFirstPageNoMore = !mFirstPageNoMore;
                    if (!mFirstError) {
                        mFirstError = true;
                    }
                } else if (mPage == 4) {
                    size = 1;
                }

                final int dataSize = size;
                mHandler.postTask(new Runnable() {
                    @Override
                    public void run() {
                        mCallBack.success(DataServer.getSampleData(dataSize));
                    }
                });
            }
        }
    }
}
