package com.talkweb.brvah.slice;

import com.talkweb.brvah.ResourceTable;
import com.talkweb.brvah.adapter.SectionMultipleItemAdapter;
import com.talkweb.brvah.base.BaseSlice;
import com.talkweb.brvah.data.DataServer;
import com.talkweb.brvah.entity.SectionMultipleItem;
import com.talkweb.library.adapter.base.BaseQuickAdapter;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Component;
import ohos.agp.components.ListContainer;
import ohos.agp.window.dialog.ToastDialog;

import java.util.List;

public class SectionMultipleItemUseAbilitySlice extends BaseSlice {


    @Override
    public int getLayout() {
        return ResourceTable.Layout_ability_section_use;
    }

    @Override
    protected void initWidget() {
        setBackBtn();
        setTitle("SectionMultiple Use");
        ListContainer mRecyclerView = (ListContainer) findComponentById(ResourceTable.Id_list_section);
        //mRecyclerView.setLayoutManager(new LinearLayoutManager(this));

        // 1. create entityList which item data extend SectionMultiEntity
        List<SectionMultipleItem> mData = DataServer.getSectionMultiData();

        // create adapter which extend BaseSectionMultiItemQuickAdapter provide your headerResId
        SectionMultipleItemAdapter sectionAdapter = new SectionMultipleItemAdapter(ResourceTable.Layout_def_section_head, mData);
        sectionAdapter.setOnItemChildClickListener(new BaseQuickAdapter.OnItemChildClickListener() {
            @Override
            public void onItemChildClick(BaseQuickAdapter adapter, Component view, int position) {
                SectionMultipleItem item = (SectionMultipleItem) adapter.getData().get(position);
                switch (view.getId()) {
                    case ResourceTable.Id_card_view:
                        // 获取主体item相应数据给后期使用
                        if (item.getVideo() != null) {
                           new ToastDialog(SectionMultipleItemUseAbilitySlice.this)
                                   .setText(item.getVideo().getName())
                                   .show();
                        }
                        break;
                    default:
                        new ToastDialog(SectionMultipleItemUseAbilitySlice.this)
                                .setText("OnItemChildClickListener " + position)
                                .show();
                        break;

                }
            }
        });
        mRecyclerView.setItemProvider(sectionAdapter);
    }

    @Override
    protected void initData() {

    }

}
