package com.talkweb.brvah.slice;

import com.talkweb.brvah.ResourceTable;
import com.talkweb.brvah.adapter.ExpandableItemAdapter;
import com.talkweb.brvah.base.BaseSlice;
import com.talkweb.brvah.entity.Level0Item;
import com.talkweb.brvah.entity.Level1Item;
import com.talkweb.brvah.entity.Person;
import com.talkweb.library.adapter.base.entity.MultiItemEntity;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class ExpandableUseAbilitySlice extends BaseSlice {

    private ListContainer mRecyclerView;
    private ExpandableItemAdapter adapter;
    private ArrayList<MultiItemEntity> list;
    private Checkbox expandCB;

    @Override
    public int getLayout() {
        return ResourceTable.Layout_ability_expandable_use;
    }

    @Override
    protected void initWidget() {
        setBackBtn();
        setTitle("ExpandableItem Activity");

        expandCB = (Checkbox) findComponentById(ResourceTable.Id_cbOnlyExpandOne);
        expandCB.setCheckedStateChangedListener(new AbsButton.CheckedStateChangedListener () {
            @Override
            public void onCheckedChanged(AbsButton absButton, boolean isChecked) {
                adapter.setOnlyExpandOne(isChecked);
            }

        });

        mRecyclerView = (ListContainer) findComponentById(ResourceTable.Id_rv);

        list = generateData();
        adapter = new ExpandableItemAdapter(list);

        mRecyclerView.setItemProvider(adapter);
        //adapter.expandAll();
    }

    @Override
    protected void initData() {

    }

    private ArrayList<MultiItemEntity> generateData() {
        int lv0Count = 3;
        int lv1Count = 3;
        int personCount = 5;

        String[] nameList = {"Bob1", "Andy2", "Lily3", "Brown4", "Bruce5"};
        Random random = new Random();

        ArrayList<MultiItemEntity> res = new ArrayList<>();
        for (int i = 0; i < lv0Count; i++) {
            Level0Item lv0 = new Level0Item("This is " + i + "th item in Level 0", "subtitle of " + i);
            for (int j = 0; j < lv1Count; j++) {
                Level1Item lv1 = new Level1Item("Level 1 item: " + j, "(no animation)");
                for (int k = 0; k < personCount; k++) {
                    lv1.addSubItem(new Person(nameList[k], k));
                }
                lv0.addSubItem(lv1);
            }
            res.add(lv0);
        }

        return res;
    }

}
