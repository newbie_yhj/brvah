package com.talkweb.brvah.util;

import ohos.app.Context;
import ohos.global.resource.*;

import java.io.IOException;

public final class ResourceHelper {

    private static final int MAX_LENGTH = 127;
    private static final double BUFFER = 0.5;

    private ResourceHelper(){

    }

    public static String[] getStringArray(Context context, int resourceId){
        String[] resourceStringArray = new String[]{};
        Element element = null;
        try {
            ResourceManager resourceManager = context.getResourceManager();
            element = resourceManager.getElement(resourceId);
            resourceStringArray = element.getStringArray();
        } catch (IOException | NotExistException | WrongTypeException e) {
            e.printStackTrace();
        }
        return resourceStringArray;
    }

    public static String getString(Context context, int resourceId){
        String resourceString = "";
        Element element = null;
        try {
            ResourceManager resourceManager = context.getResourceManager();
            element = resourceManager.getElement(resourceId);
            resourceString = element.getString();
        } catch (IOException | NotExistException | WrongTypeException e) {
            e.printStackTrace();
        }
        return resourceString;
    }

    public static Resource getResource(Context context, int resourceId){
        Resource resource = null;
        try {
            ResourceManager resourceManager = context.getResourceManager();
            resource = resourceManager.getResource(resourceId);
        } catch (IOException | NotExistException e) {
            e.printStackTrace();
        }
        return resource;
    }

    public static int getColor(Context context, int resourceID) {
        try {
            return context.getResourceManager().getElement(resourceID).getColor();
        } catch (IOException | NotExistException | WrongTypeException e) {
            LogUtils.info("CommonUtils", "some exception happend");
        }
        return -1;
    }

    public static long calculateLength(CharSequence str) {
        double len = 0;
        for (int i = 0; i < str.length(); i++) {
            int tmp = str.charAt(i);
            if (tmp > 0 && tmp < MAX_LENGTH) {
                len += BUFFER;
            } else {
                len++;
            }
        }
        return Math.round(len);
    }


}
