package com.talkweb.brvah.adapter;


import com.talkweb.brvah.ResourceTable;
import com.talkweb.brvah.entity.Status;
import com.talkweb.library.adapter.base.BaseQuickAdapter;
import com.talkweb.library.adapter.base.BaseViewHolder;
import ohos.agp.components.Component;
import ohos.agp.components.Text;

import java.util.ArrayList;
import java.util.List;

/**
 * 文 件 名: AnimationAdapter
 * 创 建 人: Allen
 * 创建日期: 16/12/24 15:33
 * 邮   箱: AllenCoder@126.com
 * 修改时间：
 * 修改备注：
 */
public class HeaderAndFooterAdapter extends BaseQuickAdapter<Status, BaseViewHolder> {
    public HeaderAndFooterAdapter(int dataSize) {
        super(ResourceTable.Layout_item_animation, getSampleData(dataSize));
    }

    @Override
    protected void convert( BaseViewHolder helper, Status item) {
//        helper.addOnClickListener(R.id.img).addOnClickListener(R.id.tweetName);
//        switch (helper.getLayoutPosition() % 3) {
//            case 0:
//                helper.setImageResource(R.id.img, R.mipmap.animation_img1);
//                break;
//            case 1:
//                helper.setImageResource(R.id.img, R.mipmap.animation_img2);
//                break;
//            case 2:
//                helper.setImageResource(R.id.img, R.mipmap.animation_img3);
//                break;
//            default:
//                break;
//        }
//        helper.setText(R.id.tweetName, "Hoteis in Rio de Janeiro");
//        String msg = "\"He was one of Australia's most of distinguished artistes, renowned for his portraits\"";
//        ((TextView) helper.getView(R.id.tweetText)).setText(SpannableStringUtils.getBuilder(msg).append("landscapes and nedes").setClickSpan(clickableSpan).create());
//        ((TextView) helper.getView(R.id.tweetText)).setMovementMethod(ClickableMovementMethod.getInstance());
//        ((TextView) helper.getView(R.id.tweetText)).setFocusable(false);
//        ((TextView) helper.getView(R.id.tweetText)).setClickable(false);
        ((Text) helper.getView(ResourceTable.Id_text)).setText(helper.getPosition()+"");
    }

//    private ClickableSpan clickableSpan = new ClickableSpan() {
//        @Override
//        public void onClick(View widget) {
//            ToastUtils.showShortToast("事件触发了 landscapes and nedes");
//        }
//
//        @Override
//        public void updateDrawState(TextPaint ds) {
//            ds.setColor(Utils.getContext().getResources().getColor(R.color.clickspan_color));
//            ds.setUnderlineText(true);
//        }
//    };

    public static List<Status> getSampleData(int lenth) {
        List<Status> list = new ArrayList<>();
        for (int i = 0; i < lenth; i++) {
            Status status = new Status();
            status.setUserName("Chad" + i);
            status.setCreatedAt("04/05/" + i);
            status.setRetweet(i % 2 == 0);
            status.setUserAvatar("https://avatars1.githubusercontent.com/u/7698209?v=3&s=460");
            status.setText("BaseRecyclerViewAdpaterHelper https://www.recyclerview.org");
            list.add(status);
        }
        return list;
    }

//    @Override
//    public void onComponentUnboundFromWindow(Component component) {
//
//    }
}
