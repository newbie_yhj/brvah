package com.talkweb.brvah.adapter;

import com.talkweb.brvah.ResourceTable;
import com.talkweb.brvah.entity.ClickEntity;
import com.talkweb.brvah.util.DialogUtil;
import com.talkweb.library.adapter.base.BaseMultiItemQuickAdapter;
import com.talkweb.library.adapter.base.BaseQuickAdapter;
import com.talkweb.library.adapter.base.BaseViewHolder;
import ohos.agp.components.Component;
import ohos.agp.components.ListContainer;
import ohos.agp.text.RichTextBuilder;

import java.util.List;

import static com.talkweb.brvah.entity.ClickEntity.CLICK_ITEM_CHILD_VIEW;
import static com.talkweb.brvah.entity.ClickEntity.CLICK_ITEM_VIEW;

/**
 * @author: jun.xiong
 * @date: 2021/5/13 09:55
 * @description:
 */
public class ItemClickAdapter extends BaseMultiItemQuickAdapter<ClickEntity, BaseViewHolder>
        implements BaseQuickAdapter.OnItemClickListener, BaseQuickAdapter.OnItemChildClickListener {

    private QuickAdapter nestAdapter ;

    /**
     * Same as QuickAdapter#QuickAdapter(Context,int) but with
     * some initialization data.
     *
     * @param data A new list is created out of this one to avoid mutable list
     */
    public ItemClickAdapter(List<ClickEntity> data) {
        super(data);
        addItemType(CLICK_ITEM_VIEW, ResourceTable.Layout_item_click_view);
        addItemType(CLICK_ITEM_CHILD_VIEW, ResourceTable.Layout_item_click_childview);
        addItemType(ClickEntity.LONG_CLICK_ITEM_VIEW, ResourceTable.Layout_item_click_view);
        addItemType(ClickEntity.LONG_CLICK_ITEM_CHILD_VIEW, ResourceTable.Layout_item_click_childview);
        addItemType(ClickEntity.NEST_CLICK_ITEM_CHILD_VIEW, ResourceTable.Layout_item_nest_click_view);
    }

    @Override
    protected void convert(BaseViewHolder helper, ClickEntity item) {
        switch (helper.getItemViewType()) {
            case ClickEntity.CLICK_ITEM_VIEW:
                helper.setText(ResourceTable.Id_tv, item.getTitle());
                helper.addOnClickListener(ResourceTable.Id_btn);
                break;
            case ClickEntity.CLICK_ITEM_CHILD_VIEW:
                helper.setText(ResourceTable.Id_tv, item.getTitle());
                helper.addOnClickListener(ResourceTable.Id_tv_reduce).addOnClickListener(ResourceTable.Id_tv_add)
                        .addOnLongClickListener(ResourceTable.Id_tv_add).addOnLongClickListener(ResourceTable.Id_tv_reduce);
                // set img data
                break;
            case ClickEntity.LONG_CLICK_ITEM_VIEW:
                helper.setText(ResourceTable.Id_tv, item.getTitle());
                helper.setText(ResourceTable.Id_btn,"LongClick");
                helper.addOnLongClickListener(ResourceTable.Id_btn);
                break;
            case ClickEntity.LONG_CLICK_ITEM_CHILD_VIEW:
                helper.setText(ResourceTable.Id_tv, item.getTitle());
                helper.addOnLongClickListener(ResourceTable.Id_tv_reduce).addOnLongClickListener(ResourceTable.Id_tv_add)
                        .addOnClickListener(ResourceTable.Id_tv_reduce).addOnClickListener(ResourceTable.Id_tv_add);
                break;
            case ClickEntity.NEST_CLICK_ITEM_CHILD_VIEW:
                // u can set nestview id
                helper.setNestView(ResourceTable.Id_item_nest_click);
                final ListContainer recyclerView = helper.getView(ResourceTable.Id_item_list);
                nestAdapter = new QuickAdapter(10);
                nestAdapter.setOnItemClickListener(this);
                nestAdapter.setOnItemChildClickListener(this);
                recyclerView.setItemProvider(nestAdapter);

                RichTextBuilder builder = new RichTextBuilder();
                builder.addText("aaa");
                break;
            default:
                break;
        }
    }

    @Override
    public void onItemChildClick(BaseQuickAdapter adapter, Component view, int position) {
        DialogUtil.toastShort(view.getContext(), "Nest onChildClick:" + position);
    }

    @Override
    public void onItemClick(BaseQuickAdapter adapter, Component view, int position) {
        DialogUtil.toastShort(view.getContext(), "Nest onItemClick:" + position);
    }
}
