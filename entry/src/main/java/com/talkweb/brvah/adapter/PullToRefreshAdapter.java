package com.talkweb.brvah.adapter;

import com.talkweb.brvah.ResourceTable;
import com.talkweb.brvah.data.DataServer;
import com.talkweb.brvah.entity.Status;
import com.talkweb.library.adapter.base.BaseQuickAdapter;
import com.talkweb.library.adapter.base.BaseViewHolder;

import java.util.List;

/**
 * @author: jun.xiong
 * @date: 2021/5/18 15:41
 * @description:
 */
public class PullToRefreshAdapter extends BaseQuickAdapter<Status, BaseViewHolder> {


    public PullToRefreshAdapter() {
        super(ResourceTable.Layout_item_image_text_view, DataServer.getSampleData(2));
    }

    @Override
    protected void convert(BaseViewHolder helper, Status item) {
        switch (helper.getPosition() % 3) {
            case 0:
                helper.setImageResource(ResourceTable.Id_iv, ResourceTable.Media_animation_img1);
                break;
            case 1:
                helper.setImageResource(ResourceTable.Id_iv, ResourceTable.Media_animation_img2);
                break;
            case 2:
                helper.setImageResource(ResourceTable.Id_iv, ResourceTable.Media_animation_img3);
                break;
            default:
                break;
        }
        helper.setText(ResourceTable.Id_tv, item.getUserName());
    }
}
