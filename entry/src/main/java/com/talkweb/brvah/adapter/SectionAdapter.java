package com.talkweb.brvah.adapter;


import com.talkweb.brvah.ResourceTable;
import com.talkweb.brvah.entity.MySection;
import com.talkweb.brvah.entity.Video;
import com.talkweb.library.adapter.base.BaseSectionQuickAdapter;
import com.talkweb.library.adapter.base.BaseViewHolder;

import java.util.List;

/**
 * https://github.com/CymChad/BaseRecyclerViewAdapterHelper
 */
public class SectionAdapter extends BaseSectionQuickAdapter<MySection, BaseViewHolder> {
    /**
     * Same as QuickAdapter#QuickAdapter(Context,int) but with
     * some initialization data.
     *
     * @param sectionHeadResId The section head layout id for each item
     * @param layoutResId      The layout resource id of each item.
     * @param data             A new list is created out of this one to avoid mutable list
     */
    public SectionAdapter(int layoutResId, int sectionHeadResId, List data) {
        super(layoutResId, sectionHeadResId, data);
    }

    @Override
    protected void convertHead(BaseViewHolder helper, final MySection item) {
        helper.setText(ResourceTable.Id_head, item.header);
        helper.setVisible(ResourceTable.Id_more, item.isMore());
        helper.addOnClickListener(ResourceTable.Id_more);
    }


    @Override
    protected void convert( BaseViewHolder helper, MySection item) {
        Video video = (Video) item.t;
        switch (helper.getPosition() % 2) {
            case 0:
                helper.setImageResource(ResourceTable.Id_iv, ResourceTable.Media_m_img1);
                break;
            case 1:
                helper.setImageResource(ResourceTable.Id_iv, ResourceTable.Media_m_img2);
                break;
            default:
                break;

        }
        helper.setText(ResourceTable.Id_tv, video.getName());
    }

//    @Override
//    public void onComponentUnboundFromWindow(Component component) {
//
//    }
}
