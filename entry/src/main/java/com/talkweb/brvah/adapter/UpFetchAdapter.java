package com.talkweb.brvah.adapter;


import com.talkweb.brvah.ResourceTable;
import com.talkweb.brvah.entity.Movie;
import com.talkweb.brvah.util.LogUtils;
import com.talkweb.library.adapter.base.BaseQuickAdapter;
import com.talkweb.library.adapter.base.BaseViewHolder;
import ohos.agp.components.Component;
import ohos.agp.window.dialog.ToastDialog;

import java.util.List;

/**
 * Created by tysheng
 * Date: 2017/5/25 10:47.
 * Email: tyshengsx@gmail.com
 */

public class UpFetchAdapter extends BaseQuickAdapter<Movie, BaseViewHolder> {

    public UpFetchAdapter(int layoutResId, List<Movie> data) {
        super(layoutResId, data);
    }

    @Override
    protected void convert(BaseViewHolder helper, Movie item) {
        LogUtils.info("convert:" + item.name);
        helper.setText(ResourceTable.Id_movie_name, item.name)
                .setText(ResourceTable.Id_movie_content, item.content)
                .setText(ResourceTable.Id_movie_length, item.length + "minute")
                .setText(ResourceTable.Id_movie_price, "$ " + item.price);

        helper.getView(ResourceTable.Id_button).setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                new ToastDialog(mContext)
                        .setText("buy ticket: " + item.name)
                        .show();
                LogUtils.info("buy ticket: " + item.name + ", position : " + helper.getPosition());
            }
        });
    }
}
