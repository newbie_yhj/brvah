package com.talkweb.brvah.adapter;


import androidx.annotation.NonNull;
import com.talkweb.brvah.ResourceTable;
import com.talkweb.brvah.entity.MultipleItem;
import com.talkweb.brvah.entity.SectionMultipleItem;
import com.talkweb.library.adapter.base.BaseSectionMultiItemQuickAdapter;
import com.talkweb.library.adapter.base.BaseViewHolder;

import java.util.List;

/**
 * https://github.com/CymChad/BaseRecyclerViewAdapterHelper
 */
public class SectionMultipleItemAdapter extends BaseSectionMultiItemQuickAdapter<SectionMultipleItem, BaseViewHolder> {
    /**
     * init SectionMultipleItemAdapter
     * 1. add your header resource layout
     * 2. add some kind of items
     *
     * @param sectionHeadResId The section head layout id for each item
     * @param data             A new list is created out of this one to avoid mutable list
     */
    public SectionMultipleItemAdapter(int sectionHeadResId, List data) {
        super(sectionHeadResId, data);
        addItemType(SectionMultipleItem.TEXT, ResourceTable.Layout_item_text_view);
        addItemType(SectionMultipleItem.IMG_TEXT, ResourceTable.Layout_item_img_text_view);
    }

    @Override
    protected void convertHead(BaseViewHolder helper, final SectionMultipleItem item) {
        // deal with header viewHolder
        helper.setText(ResourceTable.Id_head, item.header);
        helper.setVisible(ResourceTable.Id_more, item.isMore());
        helper.addOnClickListener(ResourceTable.Id_more);
    }

    @Override
    protected void convert(@NonNull BaseViewHolder helper, SectionMultipleItem item) {
        // deal with multiple type items viewHolder
        helper.addOnClickListener(ResourceTable.Id_card_view);
        switch (helper.getItemViewType()) {
            case MultipleItem.TEXT:
                helper.setText(ResourceTable.Id_tv, item.getVideo().getName());
                break;
            case MultipleItem.IMG_TEXT:
                switch (helper.getPosition() % 2) {
                    case 0:
                        helper.setImageResource(ResourceTable.Id_iv, ResourceTable.Media_animation_img1);
                        break;
                    case 1:
                        helper.setImageResource(ResourceTable.Id_iv, ResourceTable.Media_animation_img2);
                        break;
                    default:
                        break;
                }
            default:
                break;
        }
    }
}
