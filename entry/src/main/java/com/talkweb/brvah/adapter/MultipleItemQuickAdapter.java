package com.talkweb.brvah.adapter;

import com.talkweb.brvah.ResourceTable;
import com.talkweb.brvah.entity.MultipleItem;
import com.talkweb.library.adapter.base.BaseMultiItemQuickAdapter;
import com.talkweb.library.adapter.base.BaseViewHolder;

import java.util.List;

public class MultipleItemQuickAdapter extends BaseMultiItemQuickAdapter<MultipleItem, BaseViewHolder> {

    public MultipleItemQuickAdapter(List<MultipleItem> data) {
        super(data);

        addItemType(MultipleItem.TEXT, ResourceTable.Layout_item_text_view);
        addItemType(MultipleItem.IMG, ResourceTable.Layout_item_image_view);
        addItemType(MultipleItem.IMG_TEXT, ResourceTable.Layout_item_img_text_view);
    }

    @Override
    protected void convert(BaseViewHolder helper, MultipleItem item) {
        switch (helper.getItemViewType()) {
            case MultipleItem.TEXT:
                helper.setText(ResourceTable.Id_tv, item.getContent());
                break;
            case MultipleItem.IMG_TEXT:
                switch (helper.getPosition() % 2) {
                    case 0:
                        helper.setImageResource(ResourceTable.Id_iv, ResourceTable.Media_animation_img1);
                        break;
                    case 1:
                        helper.setImageResource(ResourceTable.Id_iv, ResourceTable.Media_animation_img2);
                        break;
                    default:
                        break;

                }
                break;
            default:
                break;
        }
    }
}
