package com.talkweb.brvah.entity;

import com.talkweb.library.adapter.base.entity.SectionEntity;

/**
 * @author: jun.xiong
 * @date: 2021/5/10 11:31
 * @description:
 */
public class MySection extends SectionEntity<Video> {

    private boolean isMore;

    public MySection(boolean isHeader, String header, boolean isMroe) {
        super(isHeader, header);
        this.isMore = isMroe;
    }

    public MySection(Video t) {
        super(t);
    }

    public boolean isMore() {
        return isMore;
    }

    public void setMore(boolean mroe) {
        isMore = mroe;
    }

}
