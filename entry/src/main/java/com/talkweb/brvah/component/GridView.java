package com.talkweb.brvah.component;

import com.talkweb.brvah.adapter.GridAdapter;
import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.agp.components.TableLayout;
import ohos.app.Context;

/**
 * The GridView
 */
public class GridView extends TableLayout {

    public interface GridClickListener {
        void onItemClick(Component component, int position);
    }

    public GridView(Context context) {
        super(context);
    }

    public GridView(Context context, AttrSet attrSet) {
        super(context, attrSet);
    }

    public GridView(Context context, AttrSet attrSet, String styleName) {
        super(context, attrSet, styleName);
    }

    /**
     * The setAdapter
     *
     * @param adapter adapter
     */
    public void setAdapter(GridAdapter adapter, GridClickListener clickedListener) {
        removeAllComponents();
        for (int i = 0, j = adapter.getComponentList().size(); i < j; i++) {
            int finalI = i;
            adapter.getComponentList().get(i).setClickedListener(new ClickedListener() {
                @Override
                public void onClick(Component component) {
                    if (clickedListener != null) {
                        clickedListener.onItemClick(component, finalI);
                    }
                }
            });
            addComponent(adapter.getComponentList().get(i));
        }
    }
}
