package com.talkweb.brvah;

import com.talkweb.brvah.slice.MainAbilitySlice;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;

public class MainAbility extends Ability {
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setMainRoute(MainAbilitySlice.class.getName());
        setTransitionAnimation(0,0);


       // HeaderAndFooterAbility.start(this);
    }
}
