package com.talkweb.brvah;

import com.talkweb.brvah.slice.MultipleItemUseAbilitySlice;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;

public class MultipleItemUseAbility extends Ability {
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setMainRoute(MultipleItemUseAbilitySlice.class.getName());
    }
}
