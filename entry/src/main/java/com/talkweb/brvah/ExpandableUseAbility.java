package com.talkweb.brvah;

import com.talkweb.brvah.slice.ExpandableUseAbilitySlice;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;

public class ExpandableUseAbility extends Ability {
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setMainRoute(ExpandableUseAbilitySlice.class.getName());
    }
}
