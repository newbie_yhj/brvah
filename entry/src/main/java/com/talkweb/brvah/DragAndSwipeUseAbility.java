package com.talkweb.brvah;

import com.talkweb.brvah.slice.DragAndSwipeSlice;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;

/**
 * @author: jun.xiong
 * @date: 2021/5/12 15:27
 * @description:
 */
public class DragAndSwipeUseAbility extends Ability {

    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);
        setMainRoute(DragAndSwipeSlice.class.getName());
    }
}
