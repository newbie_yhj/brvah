package com.talkweb.brvah;

import com.talkweb.brvah.slice.DiffUtilAbilitySlice;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;

public class DiffUtilAbility extends Ability {
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setMainRoute(DiffUtilAbilitySlice.class.getName());
    }
}
