package com.talkweb.brvah;

import com.talkweb.brvah.slice.UpFetchUseAbilitySlice;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;

public class UpFetchUseAbility extends Ability {
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setMainRoute(UpFetchUseAbilitySlice.class.getName());
    }
}
