package com.talkweb.brvah.base;

import com.talkweb.brvah.ResourceTable;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.*;
import ohos.agp.utils.TextTool;

/**
 * @author: jun.xiong
 * @date: 2021/5/8 09:30
 * @description:
 */
public abstract class BaseSlice extends AbilitySlice implements Component.ClickedListener {


    private static final String TAG = BaseSlice.class.getSimpleName();

    private Image backImg;
    private Text title;

    private Component contentLayout;

    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);
        if (initArgs(intent)) {
            setUIContent(ResourceTable.Layout_ability_root);
            setContentView();
            initBefore();
            initWidget();
            initData();
        } else {
            onBackPressed();
        }
    }

    private void setContentView() {
        ComponentContainer cc = findView(ResourceTable.Id_root_layout);
        backImg = findView(ResourceTable.Id_left_arrow);
        title = findView(ResourceTable.Id_title);

        int layId = getLayout();
        contentLayout = LayoutScatter.getInstance(this).parse(layId, null, false);
        ComponentContainer.LayoutConfig config = new ComponentContainer.LayoutConfig(ComponentContainer.LayoutConfig.MATCH_PARENT, ComponentContainer.LayoutConfig.MATCH_PARENT);
        cc.addComponent(contentLayout, config);
    }

    /**
     * show default back button listener
     */
    protected void setBackBtn() {
        backImg.setVisibility(Component.VISIBLE);
        backImg.setClickedListener(this);
    }

    /**
     * define back button listener by user
     *
     * @param l
     */
    protected void setBackClickListener(Component.ClickedListener l) {
        backImg.setVisibility(Component.VISIBLE);
        backImg.setClickedListener(l);
    }

    /**
     * set title
     *
     * @param msg
     */
    protected void setTitle(String msg) {
        if (!TextTool.isNullOrEmpty(msg))
            title.setText(msg);
    }

    protected <T extends Component> T findView(int id) {
        if (contentLayout != null) {
            return (T) contentLayout.findComponentById(id);
        } else
            return (T) findComponentById(id);
    }

    /**
     * get layout
     *
     * @return xml
     */
    public abstract int getLayout();

    /**
     * Initialize control before starting
     */
    private void initBefore() {
    }

    /**
     * Initialize related parameters
     *
     * @param intent intent
     * @return true
     */
    private boolean initArgs(Intent intent) {
        return true;
    }

    /**
     * Initialize the control
     */
    protected abstract void initWidget();

    /**
     * Initialize the data
     */
    protected abstract void initData();

    @Override
    public void onClick(Component component) {
        if (component.getId() == ResourceTable.Id_left_arrow) {
            terminateAbility();
        }
    }
}
