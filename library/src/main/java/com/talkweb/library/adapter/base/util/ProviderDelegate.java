package com.talkweb.library.adapter.base.util;


import com.talkweb.library.adapter.base.provider.BaseItemProvider;

import java.util.HashMap;

/**
 * https://github.com/chaychan
 *
 * @author ChayChan
 * @date 2018/3/21  11:04
 */

public class ProviderDelegate {

    private HashMap<Integer, BaseItemProvider> mItemProviders = new HashMap<>();

    public void registerProvider(BaseItemProvider provider) {
        if (provider == null) {
            throw new ItemProviderException("ItemProvider can not be null");
        }

        int viewType = provider.viewType();

        mItemProviders.putIfAbsent(viewType, provider);
    }

    public HashMap<Integer, BaseItemProvider> getItemProviders() {
        return mItemProviders;
    }

}
