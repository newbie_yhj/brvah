package com.talkweb.library.adapter.base.animation;

import ohos.agp.animation.Animator;
import ohos.agp.components.Component;

/**
 * https://github.com/CymChad/BaseRecyclerViewAdapterHelper
 */
public interface BaseAnimation {
    Animator[] getAnimators(Component view);
}
