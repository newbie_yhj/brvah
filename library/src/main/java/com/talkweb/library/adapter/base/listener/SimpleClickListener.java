package com.talkweb.library.adapter.base.listener;

//import android.support.v4.view.GestureDetectorCompat;
//import android.view.GestureDetector;
//import android.view.HapticFeedbackConstants;

import com.talkweb.library.adapter.base.BaseQuickAdapter;
import com.talkweb.library.adapter.base.BaseViewHolder;
import ohos.agp.components.Component;
import ohos.agp.components.ListContainer;
import ohos.multimodalinput.event.TouchEvent;

import java.util.Set;

import static com.talkweb.library.adapter.base.BaseQuickAdapter.EMPTY_VIEW;
import static com.talkweb.library.adapter.base.BaseQuickAdapter.FOOTER_VIEW;
import static com.talkweb.library.adapter.base.BaseQuickAdapter.HEADER_VIEW;
import static com.talkweb.library.adapter.base.BaseQuickAdapter.LOADING_VIEW;

/**
 * Created by AllenCoder on 2016/8/03.
 * <p>
 * This can be useful for applications that wish to implement various forms of click and longclick and childComponent click
 * manipulation of item views within the ListContainer. SimpleClickListener may intercept
 * a touch interaction already in progress even if the SimpleClickListener is already handling that
 * gesture stream itself for the purposes of scrolling.
 *
 */
public abstract class SimpleClickListener implements ListContainer.ItemClickedListener, Component.TouchEventListener {
    public static String TAG = "SimpleClickListener";

//    private GestureDetectorCompat mGestureDetector;
    private ListContainer recyclerComponent;
    protected BaseQuickAdapter baseQuickAdapter;
    private boolean mIsPrepressed = false;
    private boolean mIsShowPress = false;
    private Component mPressedComponent = null;

    public boolean onInterceptTouchEvent(ListContainer rv, TouchEvent e) {
        if (recyclerComponent == null) {
            this.recyclerComponent = rv;
            this.baseQuickAdapter = (BaseQuickAdapter) recyclerComponent.getItemProvider();
//            mGestureDetector = new GestureDetectorCompat(recyclerComponent.getContext(), new ItemTouchHelperGestureListener(recyclerComponent));
        } else if (recyclerComponent != rv) {
            this.recyclerComponent = rv;
            this.baseQuickAdapter = (BaseQuickAdapter) recyclerComponent.getItemProvider();
//            mGestureDetector = new GestureDetectorCompat(recyclerComponent.getContext(), new ItemTouchHelperGestureListener(recyclerComponent));
        }
//        if (!mGestureDetector.onTouchEvent(e) && e.getAction() == TouchEvent.PRIMARY_POINT_UP && mIsShowPress) {
//            if (mPressedComponent != null) {
//                BaseViewHolder vh = (BaseViewHolder) recyclerComponent.getChildComponentHolder(mPressedComponent);
//                if (vh == null || !isHeaderOrFooterComponent(vh.gett())) {
//                    mPressedComponent.setPressed(false);
//                }
//            }
//            mIsShowPress = false;
//            mIsPrepressed = false;
//        }
        return false;
    }


    @Override
    public boolean onTouchEvent(Component rv, TouchEvent e) {
//        mGestureDetector.onTouchEvent(e);
        return false ;
    }


//    private class ItemTouchHelperGestureListener implements GestureDetector.OnGestureListener {
//
//        private ListContainer recyclerComponent;
//
//        @Override
//        public boolean onDown(TouchEvent e) {
//            mIsPrepressed = true;
//            mPressedComponent = recyclerComponent.findChildComponentUnder(e.getX(), e.getY());
//            return false;
//        }
//
//        @Override
//        public void onShowPress(TouchEvent e) {
//            if (mIsPrepressed && mPressedComponent != null) {
//                mIsShowPress = true;
//            }
//        }
//
//        ItemTouchHelperGestureListener(ListContainer recyclerComponent) {
//            this.recyclerComponent = recyclerComponent;
//        }
//
//        @Override
//        public boolean onSingleTapUp(TouchEvent e) {
//            if (mIsPrepressed && mPressedComponent != null) {
//                if (recyclerComponent.getScrollState() != ListContainer.SCROLL_STATE_IDLE) {
//                    return false;
//                }
//                final Component pressedComponent = mPressedComponent;
//                BaseComponentHolder vh = (BaseComponentHolder) recyclerComponent.getChildComponentHolder(pressedComponent);
//
//                int position = vh.getAdapterPosition();
//                if(position == ListContainer.NO_POSITION){
//                    return false;
//                }
//                if (isHeaderOrFooterPosition(position)) {
//                    return false;
//                }
//                position -= baseQuickAdapter.getHeaderLayoutCount();
//
//                Set<Integer> childClickComponentIds = vh.getChildClickComponentIds();
//                Set<Integer> nestComponentIds = vh.getNestComponents();
//                if (childClickComponentIds != null && childClickComponentIds.size() > 0) {
//                    for (Integer childClickComponentId : childClickComponentIds) {
//                        Component childComponent = pressedComponent.findComponentById(childClickComponentId);
//                        if (childComponent != null) {
//                            if (inRangeOfComponent(childComponent, e) && childComponent.isEnabled()) {
//                                if (nestComponentIds != null && nestComponentIds.contains(childClickComponentId)) {
//                                    return false;
//                                }
//                                setPressComponentHotSpot(e, childComponent);
//                                childComponent.setPressed(true);
//                                onItemChildClick(baseQuickAdapter, childComponent, position);
//                                resetPressedComponent(childComponent);
//                                return true;
//                            } else {
//                                childComponent.setPressed(false);
//                            }
//                        }
//                    }
//                    setPressComponentHotSpot(e, pressedComponent);
//                    mPressedComponent.setPressed(true);
//                    for (Integer childClickComponentId : childClickComponentIds) {
//                        Component childComponent = pressedComponent.findComponentById(childClickComponentId);
//                        if (childComponent != null) {
//                            childComponent.setPressed(false);
//                        }
//                    }
//                    onItemClick(baseQuickAdapter, pressedComponent, position);
//                } else {
//                    setPressComponentHotSpot(e, pressedComponent);
//                    mPressedComponent.setPressed(true);
//                    if (childClickComponentIds != null && childClickComponentIds.size() > 0) {
//                        for (Integer childClickComponentId : childClickComponentIds) {
//                            Component childComponent = pressedComponent.findComponentById(childClickComponentId);
//                            if (childComponent != null) {
//                                childComponent.setPressed(false);
//                            }
//                        }
//                    }
//                    onItemClick(baseQuickAdapter, pressedComponent, position);
//                }
//                resetPressedComponent(pressedComponent);
//
//            }
//            return true;
//        }
//
//        private void resetPressedComponent(final Component pressedComponent) {
//            if (pressedComponent != null) {
//                pressedComponent.postDelayed(new Runnable() {
//                    @Override
//                    public void run() {
//                        if (pressedComponent != null) {
//                            pressedComponent.setPressed(false);
//                        }
//
//                    }
//                }, 50);
//            }
//
//            mIsPrepressed = false;
//            mPressedComponent = null;
//        }
//
//        @Override
//        public void onLongPress(TouchEvent e) {
//            boolean isChildLongClick = false;
//            if (recyclerComponent.getScrollState() != ListContainer.SCROLL_STATE_IDLE) {
//                return;
//            }
//            if (mIsPrepressed && mPressedComponent != null) {
//                mPressedComponent.performHapticFeedback(HapticFeedbackConstants.LONG_PRESS);
//                BaseComponentHolder vh = (BaseComponentHolder) recyclerComponent.getChildComponentHolder(mPressedComponent);
//                int position = vh.getAdapterPosition();
//                if (position == ListContainer.NO_POSITION) {
//                    return;
//                }
//                if (!isHeaderOrFooterPosition(position)) {
//                    Set<Integer> longClickComponentIds = vh.getItemChildLongClickComponentIds();
//                    Set<Integer> nestComponentIds = vh.getNestComponents();
//                    if (longClickComponentIds != null && longClickComponentIds.size() > 0) {
//                        for (Integer longClickComponentId : longClickComponentIds) {
//                            Component childComponent = mPressedComponent.findComponentById(longClickComponentId);
//                            if (inRangeOfComponent(childComponent, e) && childComponent.isEnabled()) {
//                                if (nestComponentIds != null && nestComponentIds.contains(longClickComponentId)) {
//                                    isChildLongClick = true;
//                                    break;
//                                }
//                                setPressComponentHotSpot(e, childComponent);
//                                onItemChildLongClick(baseQuickAdapter, childComponent, position - baseQuickAdapter.getHeaderLayoutCount());
//                                childComponent.setPressed(true);
//                                mIsShowPress = true;
//                                isChildLongClick = true;
//                                break;
//                            }
//                        }
//                    }
//                    if (!isChildLongClick) {
//                        onItemLongClick(baseQuickAdapter, mPressedComponent, position - baseQuickAdapter.getHeaderLayoutCount());
//                        setPressComponentHotSpot(e, mPressedComponent);
//                        mPressedComponent.setPressed(true);
//                        if (longClickComponentIds != null) {
//                            for (Integer longClickComponentId : longClickComponentIds) {
//                                Component childComponent = mPressedComponent.findComponentById(longClickComponentId);
//                                if (childComponent != null) {
//                                    childComponent.setPressed(false);
//                                }
//                            }
//                        }
//                        mIsShowPress = true;
//                    }
//                }
//            }
//        }
//
//        @Override
//        public boolean onScroll(TouchEvent e1, TouchEvent e2, float distanceX, float distanceY) {
//            return false;
//        }
//
//        @Override
//        public boolean onFling(TouchEvent e1, TouchEvent e2, float velocityX, float velocityY) {
//            return false;
//        }
//    }

    private void setPressComponentHotSpot(final TouchEvent e, final Component mPressedComponent) {
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            /**
             * when   click   Outside the region  ,mPressedComponent is null
             */
//            if (mPressedComponent != null && mPressedComponent.getBackgroundElement() != null) {
//                mPressedComponent.getBackgroundElement().setHotspot(e.getRawX(), e.getY() - mPressedComponent.getY());
//            }
//        }
    }

    /**
     * Callback method to be invoked when an item in this AdapterComponent has
     * been clicked.
     * @param adapter
     * @param view     The view within the AdapterComponent that was clicked (this
     *                 will be a view provided by the adapter)
     * @param position The position of the view in the adapter.
     */
    public abstract void onItemClick(BaseQuickAdapter adapter, Component view, int position);

    /**
     * callback method to be invoked when an item in this view has been
     * click and held
     * @param adapter
     * @param view     The view whihin the AbsListComponent that was clicked
     * @param position The position of the view int the adapter
     * @return true if the callback consumed the long click ,false otherwise
     */
    public abstract void onItemLongClick(BaseQuickAdapter adapter, Component view, int position);
    /**
     * callback method to be invoked when an itemchild in this view has been click
     * @param adapter
     * @param view     The view whihin the AbsListComponent that was clicked
     * @param position The position of the view int the adapter
     * @return true if the callback consumed the long click ,false otherwise
     */
    public abstract void onItemChildClick(BaseQuickAdapter adapter, Component view, int position);
    /**
     * callback method to be invoked when an item in this view has been
     * click and held
     * @param adapter
     * @param view     The view whihin the AbsListComponent that was clicked
     * @param position The position of the view int the adapter
     * @return true if the callback consumed the long click ,false otherwise
     */
    public abstract void onItemChildLongClick(BaseQuickAdapter adapter, Component view, int position);

    public boolean inRangeOfComponent(Component view, TouchEvent ev) {
        int[] location;
        if (view == null || !view.isComponentDisplayed()) {
            return false;
        }
        location = view.getLocationOnScreen();
        int x = location[0];
        int y = location[1];
        if (ev.getPointerPosition(0).getX() < x
                || ev.getPointerPosition(0).getX() > (x + view.getWidth())
                || ev.getPointerPosition(0).getY() < y
                || ev.getPointerPosition(0).getY() > (y + view.getHeight())) {
            return false;
        }
        return true;
    }

    private boolean isHeaderOrFooterPosition(int position) {
        /**
         *  have a headview and EMPTY_VIEW FOOTER_VIEW LOADING_VIEW
         */
        if (baseQuickAdapter == null) {
            if (recyclerComponent != null) {
                baseQuickAdapter = (BaseQuickAdapter) recyclerComponent.getItemProvider();
            } else {
                return false;
            }
        }
        int type = baseQuickAdapter.getItemComponentType(position);
        return (type == EMPTY_VIEW || type == HEADER_VIEW || type == FOOTER_VIEW || type == LOADING_VIEW);
    }

    private boolean isHeaderOrFooterComponent(int type) {
        return (type == EMPTY_VIEW || type == HEADER_VIEW || type == FOOTER_VIEW || type == LOADING_VIEW);
    }
}


