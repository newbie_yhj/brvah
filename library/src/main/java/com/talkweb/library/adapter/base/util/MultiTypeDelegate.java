package com.talkweb.library.adapter.base.util;

//import android.util.SparseIntArray;

import java.util.HashMap;
import java.util.List;

import static com.talkweb.library.adapter.base.BaseMultiItemQuickAdapter.TYPE_NOT_FOUND;

/**
 * help you to achieve multi type easily
 * <p>
 * Created by tysheng
 * Date: 2017/4/6 08:41.
 * Email: tyshengsx@gmail.com
 * <p>
 * <p>
 * more information: https://github.com/CymChad/BaseRecyclerViewAdapterHelper/issues/968
 */

public abstract class MultiTypeDelegate<T> {

    private static final int DEFAULT_VIEW_TYPE = -0xff;
    private HashMap<Integer, Integer> layouts;
    private boolean autoMode, selfMode;

    public MultiTypeDelegate(HashMap<Integer, Integer> layouts) {
        this.layouts = layouts;
    }

    public MultiTypeDelegate() {
    }

    public final int getDefItemViewType(List<T> data, int position) {
        T item = data.get(position);
        return item != null ? getItemType(item) : DEFAULT_VIEW_TYPE;
    }

    /**
     * get the item type from specific entity.
     *
     * @param t entity
     * @return item type
     */
    protected abstract int getItemType(T t);

    public final int getLayoutId(int viewType) {
        return this.layouts.get(viewType);
    }

    private void addItemType(int type, int layoutResId) {
        if (this.layouts == null) {
            this.layouts = new HashMap<>();
        }
        this.layouts.put(type, layoutResId);
    }

    /**
     * auto increase type vale, start from 0.
     *
     * @param layoutResIds layout id arrays
     * @return MultiTypeDelegate
     */
    public MultiTypeDelegate<T> registerItemTypeAutoIncrease(int... layoutResIds) {
        autoMode = true;
        checkMode(selfMode);
        for (int i = 0; i < layoutResIds.length; i++) {
            addItemType(i, layoutResIds[i]);
        }
        return this;
    }

    /**
     * set your own type one by one.
     *
     * @param type        type value
     * @param layoutResId layout id
     * @return MultiTypeDelegate
     */
    public MultiTypeDelegate<T> registerItemType(int type, int layoutResId) {
        selfMode = true;
        checkMode(autoMode);
        addItemType(type, layoutResId);
        return this;
    }

    private void checkMode(boolean mode) {
        if (mode) {
            throw new IllegalArgumentException("Don't mess two register mode");
        }
    }
}
