package com.talkweb.library.adapter.base.listener;


import com.talkweb.library.adapter.base.BaseQuickAdapter;
import ohos.agp.components.Component;

/**
 * create by: allen on 16/8/3.
 */

public abstract class OnItemLongClickListener extends SimpleClickListener {
    @Override
    public void onItemClick(BaseQuickAdapter adapter, Component view, int position) {

    }

    @Override
    public void onItemLongClick(BaseQuickAdapter adapter, Component view, int position) {
        onSimpleItemLongClick(adapter, view, position);
    }

    @Override
    public void onItemChildClick(BaseQuickAdapter adapter, Component view, int position) {

    }

    @Override
    public void onItemChildLongClick(BaseQuickAdapter adapter, Component view, int position) {
    }

    public abstract void onSimpleItemLongClick(BaseQuickAdapter adapter, Component view, int position);
}
