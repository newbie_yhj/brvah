package com.talkweb.library.adapter.base;


import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.talkweb.library.adapter.base.provider.BaseItemProvider;
import com.talkweb.library.adapter.base.util.MultiTypeDelegate;
import com.talkweb.library.adapter.base.util.ProviderDelegate;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;

import java.util.*;

/**
 * https://github.com/chaychan
 *
 * @author ChayChan
 * @description: When there are multiple entries, avoid too much business logic in convert(),Put the logic of each item in the corresponding ItemProvider
 * 当有多种条目的时候，避免在convert()中做太多的业务逻辑，把逻辑放在对应的ItemProvider中
 * @date 2018/3/21  9:55
 */

public abstract class MultipleItemRvAdapter<T, V extends BaseViewHolder> extends BaseQuickAdapter<T, V> {

    private HashMap<Integer, BaseItemProvider> mItemProviders;
    protected ProviderDelegate mProviderDelegate;
    private MultiTypeDelegate<T> mMultiTypeDelegate;

    public MultipleItemRvAdapter(@Nullable List<T> data) {
        super(data);
    }

    /**
     * 用于adapter构造函数完成参数的赋值后调用
     * Called after the assignment of the argument to the adapter constructor
     */
    public void finishInitialize() {
        mProviderDelegate = new ProviderDelegate();

        setMultiTypeDelegate(new MultiTypeDelegate<T>() {
            @Override
            protected int getItemType(T t) {
                return getViewType(t);
            }
        });

        registerItemProvider();

        mItemProviders = mProviderDelegate.getItemProviders();

        for (Map.Entry<Integer, BaseItemProvider> entry : mItemProviders.entrySet()) {
            BaseItemProvider provider = entry.getValue();
            provider.mData = mData;
            getMultiTypeDelegate().registerItemType(entry.getKey(), provider.layout());
        }
    }

    protected abstract int getViewType(T t);

    public abstract void registerItemProvider();

    @Override
    protected void bindViewClickListener(V baseViewHolder) {
        if (baseViewHolder == null) {
            return;
        }

        bindClick(baseViewHolder);

        super.bindViewClickListener(baseViewHolder);
    }

    @Override
    protected V onCreateDefViewHolder(ComponentContainer parent, int viewType) {
        if (getMultiTypeDelegate() == null) {
            throw new IllegalStateException("please use setMultiTypeDelegate first!");
        }
        int layoutId = getMultiTypeDelegate().getLayoutId(viewType);
        return createBaseViewHolder(parent, layoutId);
    }

    @Override
    protected int getDefItemViewType(int position) {
        if (getMultiTypeDelegate() == null) {
            throw new IllegalStateException("please use setMultiTypeDelegate first!");
        }
        return getMultiTypeDelegate().getDefItemViewType(mData, position);
    }

    @Override
    protected void convert(@NonNull V helper, T item) {
        int itemViewType = helper.getItemViewType();
        BaseItemProvider provider = mItemProviders.get(itemViewType);

        provider.mContext = helper.getItemView().getContext();

        int position = helper.getPosition() - getHeaderLayoutCount();
        provider.convert(helper, item, position);
    }

    @Override
    protected void convertPayloads(@NonNull V helper, T item, @NonNull List<Object> payloads) {
        int itemViewType = helper.getItemViewType();
        BaseItemProvider provider = mItemProviders.get(itemViewType);

        int position = helper.getPosition() - getHeaderLayoutCount();
        provider.convertPayloads(helper, item, position, payloads);
    }

    private void bindClick(final V helper) {
        OnItemClickListener clickListener = getOnItemClickListener();
        OnItemLongClickListener longClickListener = null ;//getOnItemLongClickListener();

        /*if (clickListener != null && longClickListener != null) {
            //如果已经设置了子条目点击监听和子条目长按监听
            // If you have set up a sub-entry click monitor and sub-entries long press listen
            return;
        }*/

        if (clickListener == null) {
            //如果没有设置点击监听，则回调给itemProvider
            //Callback to itemProvider if no click listener is set
            helper.getItemView().setClickedListener(new Component.ClickedListener() {
                @Override
                public void onClick(Component v) {
                    int position = helper.getPosition();
                    position -= getHeaderLayoutCount();

                    int itemViewType = helper.getItemViewType();
                    BaseItemProvider provider = mItemProviders.get(itemViewType);

                    provider.onClick(helper, mData.get(position), position);
                }
            });
        }

        /*if (longClickListener == null) {
            //如果没有设置长按监听，则回调给itemProvider
            // If you do not set a long press listener, callback to the itemProvider
            helper.getItemView().setLongClickedListener(new Component.LongClickedListener() {
                @Override
                public void onLongClicked(Component v) {
                    int position = helper.getPosition();
                    position -= getHeaderLayoutCount();

                    int itemViewType = helper.getItemViewType();
                    BaseItemProvider provider = mItemProviders.get(itemViewType);
                    provider.onLongClick(helper, mData.get(position), position);
                }
            });
        }*/
    }

    public void setMultiTypeDelegate(MultiTypeDelegate<T> multiTypeDelegate) {
        mMultiTypeDelegate = multiTypeDelegate;
    }

    public MultiTypeDelegate<T> getMultiTypeDelegate() {
        return mMultiTypeDelegate;
    }

}
