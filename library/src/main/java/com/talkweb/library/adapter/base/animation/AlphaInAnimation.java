package com.talkweb.library.adapter.base.animation;


import ohos.agp.animation.Animator;
import ohos.agp.animation.AnimatorProperty;
import ohos.agp.components.Component;

/**
 * https://github.com/CymChad/BaseRecyclerViewAdapterHelper
 */
public class AlphaInAnimation implements BaseAnimation {
    private static final float DEFAULT_ALPHA_FROM = 0f;
    private final float mFrom;

    public AlphaInAnimation() {
        this(DEFAULT_ALPHA_FROM);
    }

    public AlphaInAnimation(float from) {
        mFrom = from;
    }

    @Override
    public Animator[] getAnimators(Component view) {

        AnimatorProperty animatorProperty = view.createAnimatorProperty();

        final AnimatorProperty property = animatorProperty.alphaFrom(mFrom).alpha(1f);
        return new Animator[]{property};
    }
}
