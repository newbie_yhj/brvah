package com.talkweb.library.adapter.base.util.ItemTouch;

import ohos.agp.components.Component;
import ohos.agp.components.ListContainer;
import ohos.agp.render.Canvas;

public class ItemTouchUIUtilImpl implements ItemTouchUIUtil {

    public static final ItemTouchUIUtil INSTANCE =  new ItemTouchUIUtilImpl();

    @Override
    public void onDraw(Canvas c, ListContainer recyclerView, Component view, float dX, float dY,
                       int actionState, boolean isCurrentlyActive) {
        view.setTranslationX(dX);
        view.setTranslationY(dY);
    }

    private static float findMaxElevation(ListContainer recyclerView, Component itemView) {
        final int childCount = recyclerView.getChildCount();
        float max = 0;
        for (int i = 0; i < childCount; i++) {
            final Component child = recyclerView.getComponentAt(i);
            if (child == itemView) {
                continue;
            }
            /*final float elevation = ViewCompat.getElevation(child);
            if (elevation > max) {
                max = elevation;
            }*/
        }
        return max;
    }

    @Override
    public void onDrawOver(Canvas c, ListContainer recyclerView, Component view, float dX, float dY,
                           int actionState, boolean isCurrentlyActive) {
    }

    @Override
    public void clearView(Component view) {
        view.setTranslationX(0f);
        view.setTranslationY(0f);
    }

    @Override
    public void onSelected(Component view) {
    }
}
