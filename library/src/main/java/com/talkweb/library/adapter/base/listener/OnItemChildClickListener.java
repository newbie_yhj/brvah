package com.talkweb.library.adapter.base.listener;


import com.talkweb.library.adapter.base.BaseQuickAdapter;
import ohos.agp.components.Component;

/**
 * Created by AllenCoder on 2016/8/03.
 * A convenience class to extend when you only want to OnItemChildClickListener for a subset
 * of all the SimpleClickListener. This implements all methods in the
 * {@link SimpleClickListener}
 **/

public abstract class OnItemChildClickListener extends SimpleClickListener {
    @Override
    public void onItemClick(BaseQuickAdapter adapter, Component view, int position) {

    }

    @Override
    public void onItemLongClick(BaseQuickAdapter adapter, Component view, int position) {

    }

    @Override
    public void onItemChildClick(BaseQuickAdapter adapter, Component view, int position) {
        onSimpleItemChildClick(adapter, view, position);
    }

    @Override
    public void onItemChildLongClick(BaseQuickAdapter adapter, Component view, int position) {

    }

    public abstract void onSimpleItemChildClick(BaseQuickAdapter adapter, Component view, int position);
}
