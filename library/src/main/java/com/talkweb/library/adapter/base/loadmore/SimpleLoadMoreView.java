package com.talkweb.library.adapter.base.loadmore;


import com.talkweb.library.ResourceTable;

/**
 * Created by BlingBling on 2016/10/11.
 */

public final class SimpleLoadMoreView extends LoadMoreView {

    @Override
    public int getLayoutId() {
        return ResourceTable.Layout_brvah_quick_view_load_more;
    }

    @Override
    protected int getLoadingViewId() {
        return ResourceTable.Id_load_more_loading_view;
    }

    @Override
    protected int getLoadFailViewId() {
        return ResourceTable.Id_load_more_load_fail_view;
    }

    @Override
    protected int getLoadEndViewId() {
        return ResourceTable.Id_load_more_load_end_view;
    }
}
