package com.talkweb.library.adapter.base.util.ItemTouch;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.talkweb.library.adapter.base.BaseViewHolder;
import ohos.agp.animation.Animator;
import ohos.agp.animation.AnimatorProperty;
import ohos.agp.animation.AnimatorValue;
import ohos.agp.components.*;
import ohos.agp.render.Canvas;
import ohos.agp.render.render3d.ViewHolder;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ItemTouchHelper extends ItemDecoration {

    /**
     * Up direction, used for swipe & drag control.
     */
    public static final int UP = 1;

    /**
     * Down direction, used for swipe & drag control.
     */
    public static final int DOWN = 1 << 1;

    /**
     * Left direction, used for swipe & drag control.
     */
    public static final int LEFT = 1 << 2;

    /**
     * Right direction, used for swipe & drag control.
     */
    public static final int RIGHT = 1 << 3;

    // If you change these relative direction values, update Callback#convertToAbsoluteDirection,
    // Callback#convertToRelativeDirection.
    /**
     * Horizontal start direction. Resolved to LEFT or RIGHT depending on RecyclerView's layout
     * direction. Used for swipe & drag control.
     */
    public static final int START = LEFT << 2;

    /**
     * Horizontal end direction. Resolved to LEFT or RIGHT depending on RecyclerView's layout
     * direction. Used for swipe & drag control.
     */
    public static final int END = RIGHT << 2;

    /**
     * ItemTouchHelper is in idle state. At this state, either there is no related motion event by
     * the user or latest motion events have not yet triggered a swipe or drag.
     */
    public static final int ACTION_STATE_IDLE = 0;

    /**
     * A View is currently being swiped.
     */
    @SuppressWarnings("WeakerAccess")
    public static final int ACTION_STATE_SWIPE = 1;

    /**
     * A View is currently being dragged.
     */
    @SuppressWarnings("WeakerAccess")
    public static final int ACTION_STATE_DRAG = 2;

    /**
     * Animation type for views which are swiped successfully.
     */
    @SuppressWarnings("WeakerAccess")
    public static final int ANIMATION_TYPE_SWIPE_SUCCESS = 1 << 1;

    /**
     * Animation type for views which are not completely swiped thus will animate back to their
     * original position.
     */
    @SuppressWarnings("WeakerAccess")
    public static final int ANIMATION_TYPE_SWIPE_CANCEL = 1 << 2;

    /**
     * Animation type for views that were dragged and now will animate to their final position.
     */
    @SuppressWarnings("WeakerAccess")
    public static final int ANIMATION_TYPE_DRAG = 1 << 3;

    private static final String TAG = "ItemTouchHelper";

    private static final boolean DEBUG = false;

    private static final int ACTIVE_POINTER_ID_NONE = -1;

    static final int DIRECTION_FLAG_COUNT = 8;

    private static final int ACTION_MODE_IDLE_MASK = (1 << DIRECTION_FLAG_COUNT) - 1;

    static final int ACTION_MODE_SWIPE_MASK = ACTION_MODE_IDLE_MASK << DIRECTION_FLAG_COUNT;

    static final int ACTION_MODE_DRAG_MASK = ACTION_MODE_SWIPE_MASK << DIRECTION_FLAG_COUNT;

    /**
     * The unit we are using to track velocity
     */
    private static final int PIXELS_PER_SECOND = 1000;

    /**
     * Views, whose state should be cleared after they are detached from RecyclerView.
     * This is necessary after swipe dismissing an item. We wait until animator finishes its job
     * to clean these views.
     */
    final List<Component> mPendingCleanup = new ArrayList<>();

    /**
     * Re-use array to calculate dx dy for a ViewHolder
     */
    private final float[] mTmpPosition = new float[2];

    /**
     * Currently selected view holder
     */
    @SuppressWarnings("WeakerAccess") /* synthetic access */
            BaseViewHolder mSelected = null;

    /**
     * The reference coordinates for the action start. For drag & drop, this is the time long
     * press is completed vs for swipe, this is the initial touch point.
     */
    float mInitialTouchX;

    float mInitialTouchY;

    /**
     * Set when ItemTouchHelper is assigned to a RecyclerView.
     */
    private float mSwipeEscapeVelocity;

    /**
     * Set when ItemTouchHelper is assigned to a RecyclerView.
     */
    private float mMaxSwipeVelocity = 1000f;

    /**
     * The diff between the last event and initial touch.
     */
    float mDx;

    float mDy;

    private ChildDrawingOrderCallback mChildDrawingOrderCallback = null;

    public interface ChildDrawingOrderCallback {
        int onGetChildDrawingOrder(int var1, int var2);
    }

    /**
     * The coordinates of the selected view at the time it is selected. We record these values
     * when action starts so that we can consistently position it even if LayoutManager moves the
     * View.
     */
    private float mSelectedStartX;

    private float mSelectedStartY;

    /**
     * The pointer we are tracking.
     */
    @SuppressWarnings("WeakerAccess") /* synthetic access */
            int mActivePointerId = ACTIVE_POINTER_ID_NONE;

    /**
     * Developer callback which controls the behavior of ItemTouchHelper.
     */
    @NonNull
    Callback mCallback;

    /**
     * Current mode.
     */
    private int mActionState = ACTION_STATE_IDLE;

    @SuppressWarnings("WeakerAccess") /* synthetic access */
            int mSelectedFlags;

    private int mSlop;

    ListContainer mRecyclerView;

    VelocityDetector mVelocityTracker;

    private long mDragScrollStartTimeInMs;

    private Component mOverdrawChild;

    List<ItemTouchHelper.RecoverAnimation> mRecoverAnimations = new ArrayList();


    public void startDrag(@NonNull BaseViewHolder viewHolder) {
        if (!this.mCallback.hasDragFlag(this.mRecyclerView, viewHolder)) {
        } else if (viewHolder.getItemView().getComponentParent() != this.mRecyclerView) {
            Logger.getLogger("logger").log(Level.WARNING, "ItemTouchHelper", "Start drag has been called with a view holder which is not a child of the RecyclerView which is controlled by this ItemTouchHelper.");
        } else {
            this.obtainVelocityTracker();
            this.mDx = this.mDy = 0.0F;
            this.select(viewHolder, 2);
        }
    }

    void endRecoverAnimation(BaseViewHolder viewHolder, boolean override) {
        int recoverAnimSize = this.mRecoverAnimations.size();

        for(int i = recoverAnimSize - 1; i >= 0; --i) {
            ItemTouchHelper.RecoverAnimation anim = (ItemTouchHelper.RecoverAnimation)this.mRecoverAnimations.get(i);
            if (anim.mViewHolder == viewHolder) {
                anim.mOverridden |= override;
                if (!anim.mEnded) {
                    anim.cancel();
                }

                this.mRecoverAnimations.remove(i);
                return;
            }
        }
    }

    private int checkVerticalSwipe(BaseViewHolder viewHolder, int flags) {
        if ((flags & 3) != 0) {
            int dirFlag = this.mDy > 0.0F ? 2 : 1;
            float threshold;
            if (this.mVelocityTracker != null && this.mActivePointerId > -1) {
                this.mVelocityTracker.calculateCurrentVelocity(1000, mMaxSwipeVelocity, mMaxSwipeVelocity);
                threshold = this.mVelocityTracker.getHorizontalVelocity();
                float yVelocity = this.mVelocityTracker.getVerticalVelocity();
                int velDirFlag = yVelocity > 0.0F ? 2 : 1;
                float absYVelocity = Math.abs(yVelocity);
                if ((velDirFlag & flags) != 0 && velDirFlag == dirFlag && absYVelocity >= this.mSwipeEscapeVelocity && absYVelocity > Math.abs(threshold)) {
                    return velDirFlag;
                }
            }

            threshold = (float)this.mRecyclerView.getHeight() * this.mCallback.getSwipeThreshold(viewHolder);
            if ((flags & dirFlag) != 0 && Math.abs(this.mDy) > threshold) {
                return dirFlag;
            }
        }

        return 0;
    }

    private int swipeIfNecessary(BaseViewHolder viewHolder) {
        if (this.mActionState == 2) {
            return 0;
        } else {
            int originalMovementFlags = this.mCallback.getMovementFlags(this.mRecyclerView, viewHolder);
            int absoluteMovementFlags = this.mCallback.convertToAbsoluteDirection(originalMovementFlags, 0);
            int flags = (absoluteMovementFlags & '\uff00') >> 8;
            if (flags == 0) {
                return 0;
            } else {
                int originalFlags = (originalMovementFlags & '\uff00') >> 8;
                int swipeDir;
                if (Math.abs(this.mDx) > Math.abs(this.mDy)) {
                    if ((swipeDir = this.checkHorizontalSwipe(viewHolder, flags)) > 0) {
                        if ((originalFlags & swipeDir) == 0) {
                            return ItemTouchHelper.Callback.convertToRelativeDirection(swipeDir, 0);
                        }

                        return swipeDir;
                    }

                    if ((swipeDir = this.checkVerticalSwipe(viewHolder, flags)) > 0) {
                        return swipeDir;
                    }
                } else {
                    if ((swipeDir = this.checkVerticalSwipe(viewHolder, flags)) > 0) {
                        return swipeDir;
                    }

                    if ((swipeDir = this.checkHorizontalSwipe(viewHolder, flags)) > 0) {
                        if ((originalFlags & swipeDir) == 0) {
                            return ItemTouchHelper.Callback.convertToRelativeDirection(swipeDir, 0);
                        }

                        return swipeDir;
                    }
                }

                return 0;
            }
        }
    }

    private int checkHorizontalSwipe(BaseViewHolder viewHolder, int flags) {
        if ((flags & 12) != 0) {
            int dirFlag = this.mDx > 0.0F ? 8 : 4;
            float threshold;
            if (this.mVelocityTracker != null && this.mActivePointerId > -1) {
                this.mVelocityTracker.calculateCurrentVelocity(1000, mMaxSwipeVelocity, mMaxSwipeVelocity);
                threshold = this.mVelocityTracker.getHorizontalVelocity();
                float yVelocity = this.mVelocityTracker.getVerticalVelocity();
                int velDirFlag = threshold > 0.0F ? 8 : 4;
                float absXVelocity = Math.abs(threshold);
                if ((velDirFlag & flags) != 0 && dirFlag == velDirFlag && absXVelocity >= mSwipeEscapeVelocity && absXVelocity > Math.abs(yVelocity)) {
                    return velDirFlag;
                }
            }

            threshold = (float)this.mRecyclerView.getWidth() * this.mCallback.getSwipeThreshold(viewHolder);
            if ((flags & dirFlag) != 0 && Math.abs(this.mDx) > threshold) {
                return dirFlag;
            }
        }

        return 0;
    }

    private void getSelectedDxDy(float[] outPosition) {
        if ((this.mSelectedFlags & 12) != 0) {
            outPosition[0] = this.mSelectedStartX + this.mDx - (float)this.mSelected.getItemView().getLeft();
        } else {
            outPosition[0] = this.mSelected.getItemView().getTranslationX();
        }

        if ((this.mSelectedFlags & 3) != 0) {
            outPosition[1] = this.mSelectedStartY + this.mDy - (float)this.mSelected.getItemView().getTop();
        } else {
            outPosition[1] = this.mSelected.getItemView().getTranslationY();
        }

    }

    void select(@Nullable BaseViewHolder selected, int actionState) {
        if (selected != this.mSelected || actionState != this.mActionState) {
            this.mDragScrollStartTimeInMs = -9223372036854775808L;
            int prevActionState = this.mActionState;
            this.endRecoverAnimation(selected, true);
            this.mActionState = actionState;
            if (actionState == 2) {
                if (selected == null) {
                    throw new IllegalArgumentException("Must pass a ViewHolder when dragging");
                }

                this.mOverdrawChild = selected.getItemView();
//                this.addChildDrawingOrderCallback();
            }

            int actionStateMask = (1 << 8 + 8 * actionState) - 1;
            boolean preventLayout = false;
            if (this.mSelected != null) {
                final BaseViewHolder prevSelected = this.mSelected;
                if (prevSelected.getItemView().getComponentParent() != null) {
                    final int swipeDir = prevActionState == 2 ? 0 : this.swipeIfNecessary(prevSelected);
                    this.releaseVelocityTracker();
                    float targetTranslateX;
                    float targetTranslateY;
                    switch(swipeDir) {
                        case 1:
                        case 2:
                            targetTranslateX = 0.0F;
                            targetTranslateY = Math.signum(this.mDy) * (float)this.mRecyclerView.getHeight();
                            break;
                        case 4:
                        case 8:
                        case 16:
                        case 32:
                            targetTranslateY = 0.0F;
                            targetTranslateX = Math.signum(this.mDx) * (float)this.mRecyclerView.getWidth();
                            break;
                        default:
                            targetTranslateX = 0.0F;
                            targetTranslateY = 0.0F;
                    }

                    byte animationType;
                    if (prevActionState == 2) {
                        animationType = 8;
                    } else if (swipeDir > 0) {
                        animationType = 2;
                    } else {
                        animationType = 4;
                    }

                    this.getSelectedDxDy(this.mTmpPosition);
                    float currentTranslateX = this.mTmpPosition[0];
                    float currentTranslateY = this.mTmpPosition[1];
                    ItemTouchHelper.RecoverAnimation rv = new ItemTouchHelper.RecoverAnimation(prevSelected,
                            animationType, prevActionState, currentTranslateX, currentTranslateY,
                            targetTranslateX, targetTranslateY) {
                        public void onAnimationEnd(Animator animation) {
                            super.onEnd(animation);
                            if (!this.mOverridden) {
                                if (swipeDir <= 0) {
                                    ItemTouchHelper.this.mCallback.clearView(ItemTouchHelper.this.mRecyclerView, prevSelected);
                                } else {
                                    ItemTouchHelper.this.mPendingCleanup.add(prevSelected.getItemView());
                                    this.mIsPendingCleanup = true;
                                    if (swipeDir > 0) {
                                        ItemTouchHelper.this.postDispatchSwipe(this, swipeDir);
                                    }
                                }

                                if (ItemTouchHelper.this.mOverdrawChild == prevSelected.getItemView()) {
                                    ItemTouchHelper.this.removeChildDrawingOrderCallbackIfNecessary(prevSelected.getItemView());
                                }

                            }
                        }
                    };
                    long duration = this.mCallback.getAnimationDuration(this.mRecyclerView, animationType, targetTranslateX - currentTranslateX, targetTranslateY - currentTranslateY);
                    rv.setDuration(duration);
                    this.mRecoverAnimations.add(rv);
                    rv.start();
                    preventLayout = true;
                } else {
                    this.removeChildDrawingOrderCallbackIfNecessary(prevSelected.getItemView());
                    this.mCallback.clearView(this.mRecyclerView, prevSelected);
                }

                this.mSelected = null;
            }

            if (selected != null) {
                this.mSelectedFlags = (this.mCallback.getAbsoluteMovementFlags(this.mRecyclerView, selected) & actionStateMask) >> this.mActionState * 8;
                this.mSelectedStartX = (float)selected.getItemView().getLeft();
                this.mSelectedStartY = (float)selected.getItemView().getTop();
                this.mSelected = selected;
                if (actionState == 2) {
                    //this.mSelected.getItemView().performHapticFeedback(0);
                }
            }

            ComponentParent rvParent = this.mRecyclerView.getComponentParent();
            if (rvParent != null) {
                //rvParent.requestDisallowInterceptTouchEvent(this.mSelected != null);
            }

            if (!preventLayout) {
                //this.mRecyclerView.getLayoutManager().requestSimpleAnimationsInNextLayout();
            }

            this.mCallback.onSelectedChanged(this.mSelected, this.mActionState);
            this.mRecyclerView.invalidate();
        }
    }

    void removeChildDrawingOrderCallbackIfNecessary(Component view) {
        if (view == this.mOverdrawChild) {
            this.mOverdrawChild = null;
            if (this.mChildDrawingOrderCallback != null) {
                mRecyclerView.setDraggedListener(0, null);
                //this.mRecyclerView.setChildDrawingOrderCallback((ChildDrawingOrderCallback)null);
            }
        }

    }

    void postDispatchSwipe(final ItemTouchHelper.RecoverAnimation anim, final int swipeDir) {
        this.mRecyclerView.getContext().getUITaskDispatcher().syncDispatch(new Runnable() {
            public void run() {
                if (ItemTouchHelper.this.mRecyclerView != null && ItemTouchHelper.this.mRecyclerView.isBoundToWindow()
                        && !anim.mOverridden && anim.mViewHolder.getPosition() != -1) {
                    ItemTouchHelper.this.mCallback.onSwiped(anim.mViewHolder, swipeDir);
                    /*ItemAnimator animator = ItemTouchHelper.this.mRecyclerView.getItemAnimator();
                    if ((animator == null || !animator.isRunning((ItemAnimatorFinishedListener)null)) && !ItemTouchHelper.this.hasRunningRecoverAnim()) {
                        ItemTouchHelper.this.mCallback.onSwiped(anim.mViewHolder, swipeDir);
                    } else {
                        ItemTouchHelper.this.mRecyclerView.post(this);
                    }*/
                }

            }
        });
    }

    private void releaseVelocityTracker() {
        if (this.mVelocityTracker != null) {
            this.mVelocityTracker.clear();
            this.mVelocityTracker = null;
        }
    }

    void obtainVelocityTracker() {
        if (this.mVelocityTracker != null) {
            this.mVelocityTracker.clear();
        }

        this.mVelocityTracker = VelocityDetector.obtainInstance();
    }

    public abstract static class Callback {

        private int mDefaultSwipeDirs;

        private int mDefaultDragDirs;

        public long getAnimationDuration(@NonNull ListContainer recyclerView, int animationType, float animateDx, float animateDy) {
            /*ItemAnimator itemAnimator = recyclerView.getItemAnimator();
            if (itemAnimator == null) {
                return animationType == 8 ? 200L : 250L;
            } else {
                return animationType == 8 ? itemAnimator.getMoveDuration() : itemAnimator.getRemoveDuration();
            }*/
            return 200L;
        }

        public void onSelectedChanged(@Nullable BaseViewHolder viewHolder, int actionState) {
            if (viewHolder != null) {
                ItemTouchUIUtilImpl.INSTANCE.onSelected(viewHolder.getItemView());
            }
        }

        public void clearView(@NonNull ListContainer recyclerView, @NonNull BaseViewHolder viewHolder) {
            ItemTouchUIUtilImpl.INSTANCE.clearView(viewHolder.getItemView());
        }

        public abstract int getMovementFlags(@NonNull ListContainer recyclerView,
                                             @NonNull BaseViewHolder viewHolder);

        public abstract boolean onMove(@NonNull ListContainer recyclerView,
                                       @NonNull BaseViewHolder viewHolder, @NonNull BaseViewHolder target);

        public static int makeMovementFlags(int dragFlags, int swipeFlags) {
            return makeFlag(ACTION_STATE_IDLE, swipeFlags | dragFlags)
                    | makeFlag(ACTION_STATE_SWIPE, swipeFlags)
                    | makeFlag(ACTION_STATE_DRAG, dragFlags);
        }

        public float getSwipeVelocityThreshold(float defaultValue) {
            return defaultValue;
        }

        public float getSwipeThreshold(@NonNull ViewHolder viewHolder) {
            return 0.5F;
        }

        public static int convertToRelativeDirection(int flags, int layoutDirection) {
            int masked = flags & 789516;
            if (masked == 0) {
                return flags;
            } else {
                flags &= ~masked;
                if (layoutDirection == 0) {
                    flags |= masked << 2;
                    return flags;
                } else {
                    flags |= masked << 1 & -789517;
                    flags |= (masked << 1 & 789516) << 2;
                    return flags;
                }
            }
        }

        public int convertToAbsoluteDirection(int flags, int layoutDirection) {
            int masked = flags & 3158064;
            if (masked == 0) {
                return flags;
            } else {
                flags &= ~masked;
                if (layoutDirection == 0) {
                    flags |= masked >> 2;
                    return flags;
                } else {
                    flags |= masked >> 1 & -3158065;
                    flags |= (masked >> 1 & 3158064) >> 2;
                    return flags;
                }
            }
        }

        final int getAbsoluteMovementFlags(ListContainer recyclerView, BaseViewHolder viewHolder) {
            int flags = this.getMovementFlags(recyclerView, viewHolder);
            return this.convertToAbsoluteDirection(flags, 0);
        }

        boolean hasDragFlag(ListContainer recyclerView, BaseViewHolder viewHolder) {
            int flags = this.getAbsoluteMovementFlags(recyclerView, viewHolder);
            return (flags & 16711680) != 0;
        }

        public static int makeFlag(int actionState, int directions) {
            return directions << (actionState * DIRECTION_FLAG_COUNT);
        }

        public int getSwipeDirs(@SuppressWarnings("unused") @NonNull ListContainer recyclerView,
                                @NonNull @SuppressWarnings("unused") BaseViewHolder viewHolder) {
            return mDefaultSwipeDirs;
        }

        public int getDragDirs(@SuppressWarnings("unused") @NonNull ListContainer recyclerView,
                               @SuppressWarnings("unused") @NonNull BaseViewHolder viewHolder) {
            return mDefaultDragDirs;
        }

        public void setDefaultSwipeDirs(@SuppressWarnings("unused") int defaultSwipeDirs) {
            mDefaultSwipeDirs = defaultSwipeDirs;
        }

        public void setDefaultDragDirs(@SuppressWarnings("unused") int defaultDragDirs) {
            mDefaultDragDirs = defaultDragDirs;
        }

        public void onMoved(@NonNull final ListContainer recyclerView,
                            @NonNull final BaseViewHolder viewHolder, int fromPos, @NonNull final BaseViewHolder target,
                            int toPos, int x, int y) {
            final LayoutManager layoutManager = recyclerView.getLayoutManager();
            if (layoutManager instanceof ViewDropHandler) {
                ((ViewDropHandler) layoutManager).prepareForDrop(viewHolder.getItemView(),
                        target.getItemView(), x, y);
                return;
            }

            // if layout manager cannot handle it, do some guesswork

           /* if (layoutManager.canScrollHorizontally()) {
                final int minLeft = layoutManager.getDecoratedLeft(target.itemView);
                if (minLeft <= recyclerView.getPaddingLeft()) {
                    recyclerView.scrollToPosition(toPos);
                }
                final int maxRight = layoutManager.getDecoratedRight(target.itemView);
                if (maxRight >= recyclerView.getWidth() - recyclerView.getPaddingRight()) {
                    recyclerView.scrollToPosition(toPos);
                }
            }

            if (layoutManager.canScrollVertically()) {
                final int minTop = layoutManager.getDecoratedTop(target.itemView);
                if (minTop <= recyclerView.getPaddingTop()) {
                    recyclerView.scrollToPosition(toPos);
                }
                final int maxBottom = layoutManager.getDecoratedBottom(target.itemView);
                if (maxBottom >= recyclerView.getHeight() - recyclerView.getPaddingBottom()) {
                    recyclerView.scrollToPosition(toPos);
                }
            }*/
        }

        public boolean isLongPressDragEnabled() {
            return true;
        }

        public boolean isItemViewSwipeEnabled() {
            return true;
        }

        public abstract void onSwiped(@NonNull BaseViewHolder viewHolder, int direction);

        public float getMoveThreshold(@NonNull BaseViewHolder viewHolder) {
            return .5f;
        }

        public float getSwipeThreshold(@NonNull BaseViewHolder viewHolder) {
            return .5f;
        }

        public void onChildDrawOver(@NonNull Canvas c, @NonNull ListContainer recyclerView,
                                    BaseViewHolder viewHolder,
                                    float dX, float dY, int actionState, boolean isCurrentlyActive) {
            ItemTouchUIUtilImpl.INSTANCE.onDrawOver(c, recyclerView, viewHolder.getItemView(), dX, dY,
                    actionState, isCurrentlyActive);
        }
    }

    public static interface ViewDropHandler {
        void prepareForDrop(@NonNull Component view, @NonNull Component target, int x, int y);
    }

    private static class RecoverAnimation implements AnimatorValue.StateChangedListener {
        float mStartDx;
        float mStartDy;
        float mTargetX;
        float mTargetY;
        BaseViewHolder mViewHolder;
        int mActionState;
        AnimatorValue mValueAnimator;
        int mAnimationType;
        boolean mIsPendingCleanup;
        float mX;
        float mY;
        boolean mOverridden = false;
        boolean mEnded = false;
        private float mFraction;

        RecoverAnimation(BaseViewHolder viewHolder, int animationType, int actionState, float startDx, float startDy, float targetX, float targetY) {
            this.mActionState = actionState;
            this.mAnimationType = animationType;
            this.mViewHolder = viewHolder;
            this.mStartDx = startDx;
            this.mStartDy = startDy;
            this.mTargetX = targetX;
            this.mTargetY = targetY;
            AnimatorValue animator = new AnimatorValue();
            animator.setValueUpdateListener(new AnimatorValue.ValueUpdateListener() {
                @Override
                public void onUpdate(AnimatorValue animatorValue, float v) {
                    RecoverAnimation.this.setFraction(v);
                }
            });

            this.mValueAnimator.setStateChangedListener(this);
            this.setFraction(0.0F);
        }

        public void setDuration(long duration) {
            this.mValueAnimator.setDuration(duration);
        }

        public void start() {
            //this.mViewHolder.setIsRecyclable(false);
            this.mValueAnimator.start();
        }

        public void cancel() {
            this.mValueAnimator.cancel();
        }

        public void setFraction(float fraction) {
            this.mFraction = fraction;
        }

        public void update(Animator animator) {
            if (this.mStartDx == this.mTargetX) {
                this.mX = this.mViewHolder.getItemView().getTranslationX();
            } else {
                this.mX = this.mStartDx + this.mFraction * (this.mTargetX - this.mStartDx);
            }

            if (this.mStartDy == this.mTargetY) {
                this.mY = this.mViewHolder.getItemView().getTranslationY();
            } else {
                this.mY = this.mStartDy + this.mFraction * (this.mTargetY - this.mStartDy);
            }

        }

        @Override
        public void onStart(Animator animator) {

        }

        @Override
        public void onStop(Animator animator) {

        }

        @Override
        public void onCancel(Animator animator) {
            this.setFraction(1.0F);
        }

        @Override
        public void onEnd(Animator animator) {
            this.mEnded = true;
        }

        @Override
        public void onPause(Animator animator) {

        }

        @Override
        public void onResume(Animator animator) {

        }
    }
}
