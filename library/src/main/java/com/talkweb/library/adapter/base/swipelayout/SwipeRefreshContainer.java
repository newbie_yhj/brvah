package com.talkweb.library.adapter.base.swipelayout;

import ohos.agp.components.*;
import ohos.agp.utils.Point;
import ohos.app.Context;
import ohos.eventhandler.EventHandler;
import ohos.eventhandler.EventRunner;
import ohos.multimodalinput.event.TouchEvent;

/**
 * @author: jun.xiong
 * @date: 2021/5/17 11:05
 * @description:
 */
public class SwipeRefreshContainer extends ComponentContainer implements
        Component.TouchEventListener, Component.LayoutRefreshedListener, IRefresh {


    private GestureDetector mGestureDetector;
    private HeaderStackLayout.RefreshState mState;
    private IRefresh.RefreshListener mRefreshListener;
    /**
     * 下拉刷新的头部组件
     */
    protected HeaderStackLayout mHeaderStackLayout;
    private int mLastY;
    /**
     * 下拉刷新的时候是否禁止滚动
     */
    private boolean disableRefreshScroll;
    private AutoScroller mAutoScroll;

    public SwipeRefreshContainer(Context context) {
        super(context);
        init();
    }

    public SwipeRefreshContainer(Context context, AttrSet attrSet) {
        super(context, attrSet);
        init();
    }

    public SwipeRefreshContainer(Context context, AttrSet attrSet, String styleName) {
        super(context, attrSet, styleName);
        init();
    }

    Point start ;
    Point current;
    Point end;

    private void init() {
//        setTouchEventListener(this);
        setLayoutRefreshedListener(this);
        mGestureDetector = new GestureDetector(gestureDetector);
        mAutoScroll = new AutoScroller(this);

        setDraggedListener(Component.DRAG_VERTICAL, new Component.DraggedListener() {
            @Override
            public void onDragDown(Component component, DragInfo dragInfo) {
                System.out.println("按下："+ dragInfo.downPoint);
            }

            @Override
            public void onDragStart(Component component, DragInfo dragInfo) {
                System.out.println("开始:"+dragInfo.startPoint);
                start = dragInfo.startPoint;
            }

            @Override
            public void onDragUpdate(Component component, DragInfo dragInfo) {
                System.out.println("更新位置："+ dragInfo.updatePoint);
                current = dragInfo.updatePoint;
                // 处理完抬起事件后，将手势交给手势处理器
                mGestureDetector.onMove(start, current);
            }

            @Override
            public void onDragEnd(Component component, DragInfo dragInfo) {
                System.out.println("结束："+ dragInfo.updatePoint);
                Component head = getComponentAt(0);
                // 松开手
                if (head.getBottom() > 0) {
                    // 头部视图被拉下来了
                    if (mState != HeaderStackLayout.RefreshState.STATE_REFRESH) {
                        // 松开手的时候不是刷新状态
                        recover(head.getBottom());
                    }
                }
                mLastY = 0;

            }

            @Override
            public void onDragCancel(Component component, DragInfo dragInfo) {
                System.out.println("取消："+dragInfo.updatePoint);
                Component head = getComponentAt(0);
                // 松开手
                if (head.getBottom() > 0) {
                    // 头部视图被拉下来了
                    if (mState != HeaderStackLayout.RefreshState.STATE_REFRESH) {
                        // 松开手的时候不是刷新状态
                        recover(head.getBottom());
                    }
                }
                mLastY = 0;
            }
        });

        }

    @Override
    public void setDisableRefreshScroll(boolean disableRefreshScroll) {
        this.disableRefreshScroll = disableRefreshScroll;
    }

    @Override
    public void refreshFinish() {
        System.out.println("刷新完成========");
        Component head = getComponentAt(0);
        mHeaderStackLayout.onFinish();
        mHeaderStackLayout.setState(HeaderStackLayout.RefreshState.STATE_INIT);
        int bottom = head.getBottom();
        if (bottom > 0) {
            // 刷新完成隐藏头部视图
            recover(bottom);
        }
        mState = HeaderStackLayout.RefreshState.STATE_INIT;
    }

    @Override
    public void setRefreshListener(RefreshListener listener) {
        this.mRefreshListener = listener;
    }

    @Override
    public void setRefreshOverComponent(HeaderStackLayout headerStackLayout) {
        if (mHeaderStackLayout != null) {
            removeComponent(mHeaderStackLayout);
        }
        mHeaderStackLayout = headerStackLayout;
        LayoutConfig config = new LayoutConfig(LayoutConfig.MATCH_PARENT, LayoutConfig.MATCH_CONTENT);
        addComponent(mHeaderStackLayout, 0, config);
    }

    @Override
    public void onRefreshed(Component component) {
        int left = component.getLeft();
        int top = component.getTop();
        int right = component.getRight();
        int bottom = component.getBottom();
        Component head = getComponentAt(0);
        Component child = getComponentAt(1);
        if (head != null && child != null) {
            int childTop = child.getTop();
            if (mState == HeaderStackLayout.RefreshState.STATE_REFRESH) {
                head.setComponentPosition(0, mHeaderStackLayout.mPullRefreshHeight - head.getHeight(), right, mHeaderStackLayout.mPullRefreshHeight);
                child.setComponentPosition(0, mHeaderStackLayout.mPullRefreshHeight, right, mHeaderStackLayout.mPullRefreshHeight + child.getHeight());
            } else {
                head.setComponentPosition(0, childTop - head.getHeight(), right, childTop);
                child.setComponentPosition(0, childTop, right, childTop + child.getHeight());
            }

            Component other;
            // 剩下的组件能够不跟随手势移动以实现一些特殊效果，如悬浮的效果
            for (int i = 2; i < getChildCount(); ++i) {
                other = getComponentAt(i);
                other.setComponentPosition(0, top, right, bottom);
            }
        }

    }

    @Override
    public boolean onTouchEvent(Component component, TouchEvent ev) {
        return false;
        //System.out.println("onTouchEvent————Action：" + ev.getAction());
        //return mGestureDetector.onTouchEvent(ev);
        /*Component head = getComponentAt(0);
        if (ev.getAction() == TouchEvent.CANCEL ||
                ev.getAction() == TouchEvent.PRIMARY_POINT_UP) {
            System.out.println("layout---抬手");
            // 松开手
            if (head.getBottom() > 0) {
                // 头部视图被拉下来了
                if (mState != HeaderStackLayout.RefreshState.STATE_REFRESH) {
                    // 松开手的时候不是刷新状态
                    recover(head.getBottom());
                    return false;
                }
            }
            mLastY = 0;
        }
        // 处理完抬起事件后，将手势交给手势处理器
        boolean consumed = mGestureDetector.onTouchEvent(ev);
        if (head.getBottom() != 0 && consumed) {
            // 头部视图被拉下来了，并且消费了事件
            return true;
        }
        if (head.getBottom() != 0
                && mState != HeaderStackLayout.RefreshState.STATE_INIT
                && mState != HeaderStackLayout.RefreshState.STATE_REFRESH) {
            // 头部视图被拉下来了，并且当前状态不是初始状态，并且不是刷新状态
            return true;
        }
        if (consumed) {
            // 消费事件，事件不再往下传递
            return true;
        } else {
            return false;
        }*/
    }

    private void recover(int dis) {
        if (mRefreshListener != null && dis > mHeaderStackLayout.mPullRefreshHeight) {
            /*
             * 由于下拉的距离超过了下拉刷新的最小距离，所以需先要向上滚动一段距离，
             * 也就是需要滚动到mHiOverView.mPullRefreshHeight这个距离，然后才开始刷新，
             * 需要向上滚动多少距离才能到达mHiOverView.mPullRefreshHeight这个距离呢？
             * dis - mHiOverView.mPullRefreshHeight就是需要向上滚动的距离
             */
            mAutoScroll.recover(dis - mHeaderStackLayout.mPullRefreshHeight);
            mState = HeaderStackLayout.RefreshState.STATE_OVER_RELEASE;
        } else {
            // 没有达到下拉刷新的最小距离，也就是没有达到mHiOverView.mPullRefreshHeight这个距离，
            // 此时就应当直接向上滚动，也就是隐藏头部视图
            mAutoScroll.recover(dis);
        }
    }

    /**
     * 根据偏移量滚动头部组件和子组件
     *
     * @param offsetY 偏移量
     * @param auto    false手动触发滚动，true自动触发滚动
     * @return
     */
    private boolean moveDown(int offsetY, boolean auto) {
        Component head = getComponentAt(0);
        Component child = getComponentAt(1);
        int childTop = child.getTop() + offsetY;
        if (childTop <= 0) {
            // 异常情况处理
            offsetY = -child.getTop();
            // 移动到原始位置
            offsetTopAndBottom(head, offsetY);
            offsetTopAndBottom(child, offsetY);
            if (mState != HeaderStackLayout.RefreshState.STATE_REFRESH) {
                mState = HeaderStackLayout.RefreshState.STATE_INIT;
            }
        } else if (mState == HeaderStackLayout.RefreshState.STATE_REFRESH && childTop > mHeaderStackLayout.mPullRefreshHeight) {
            // 正在刷新中，禁止继续下拉
            return false;
        } else if (childTop <= mHeaderStackLayout.mPullRefreshHeight) {
            // 没有超出可刷新的距离
            if (mHeaderStackLayout.getState() != HeaderStackLayout.RefreshState.STATE_REFRESH && !auto) {
                // 头部开始显示
                mHeaderStackLayout.onVisible();
                mHeaderStackLayout.setState(HeaderStackLayout.RefreshState.STATE_VISIBLE);
                mState = HeaderStackLayout.RefreshState.STATE_VISIBLE;
            }
            offsetTopAndBottom(head, offsetY);
            offsetTopAndBottom(child, offsetY);
            if (childTop == mHeaderStackLayout.mPullRefreshHeight && mState == HeaderStackLayout.RefreshState.STATE_OVER_RELEASE) {
                // 开始刷新
                refresh();
            }
        } else {
            if (mHeaderStackLayout.getState() != HeaderStackLayout.RefreshState.STATE_OVER && !auto) {
                // 超出刷新位置
                mHeaderStackLayout.onOver();
                mHeaderStackLayout.setState(HeaderStackLayout.RefreshState.STATE_OVER);
            }
            offsetTopAndBottom(head, offsetY);
            offsetTopAndBottom(child, offsetY);
        }
        if (mHeaderStackLayout != null) {
            mHeaderStackLayout.onScroll(head.getBottom(), mHeaderStackLayout.mPullRefreshHeight);
        }
        return true;
    }

    /**
     * 开始刷新
     */
    private void refresh() {
        if (mRefreshListener != null) {
            mHeaderStackLayout.onRefresh();
            mRefreshListener.onRefresh();
            mState = HeaderStackLayout.RefreshState.STATE_REFRESH;
            mHeaderStackLayout.setState(HeaderStackLayout.RefreshState.STATE_REFRESH);
        }
    }


    /**
     * 修改组件的位置来达到滚动的效果，只修改组件的垂直位置
     *
     * @param component
     * @param offset
     */
    public void offsetTopAndBottom(Component component, int offset) {
        component.setTop(component.getTop() + offset);
        component.setBottom(component.getBottom() + offset);
        component.setComponentPosition(component.getLeft(), component.getTop(),
                component.getRight(), component.getBottom());
    }

    private final GestureDetector.OnGestureListener gestureDetector = new GestureDetector.OnGestureListener() {
        @Override
        public boolean onScroll(TouchEvent e1, TouchEvent e2, float distanceX, float distanceY) {
            if (Math.abs(distanceX) > Math.abs(distanceY) ) {
                // 发生横向滚动，不单独处理事件，让系统正常分发事件
                return false;
            }
            if (mRefreshListener != null && !mRefreshListener.enableRefresh()) {
                // 不允许下拉刷新，不单独处理事件，让系统正常分发事件
                return false;
            }
            if (disableRefreshScroll && mState == HeaderStackLayout.RefreshState.STATE_REFRESH) {
                // 刷新时禁止列表页滚动，返回true，消费事件，不让列表页滚动
                return true;
            }
            Component head = getComponentAt(0);
            Component child = ScrollUtil.findScrollableChild(SwipeRefreshContainer.this);
            if (ScrollUtil.childScrolled(child)) {
                // 列表页发生了滚动，不单独处理事件，让系统正常分发事件
                return false;
            }
            // 没有刷新或者没有达到可刷新的距离，并且头部已经划出或者下拉
            if ((mState != HeaderStackLayout.RefreshState.STATE_REFRESH || head.getBottom() <= mHeaderStackLayout.mPullRefreshHeight) &&
                    (head.getBottom() != 0 || distanceY <= 0)) {
                // 还在滑动中
                if (mState != HeaderStackLayout.RefreshState.STATE_OVER_RELEASE) {
                    int offsetY;
                    // 根据阻尼计算需要滑动的距离
                    if (head.getBottom() < mHeaderStackLayout.mPullRefreshHeight) {
                        // 没到可刷新的距离，减少阻尼
                        offsetY = (int) (mLastY / mHeaderStackLayout.minDamp);
                    } else {
                        // 达到可刷新的距离，加大阻尼
                        offsetY = (int) (mLastY / mHeaderStackLayout.maxDamp);
                    }
                    // 如果是正在刷新状态，则不允许在滑动的时候改变状态
                    // 用户手动下拉触发滑动，moveDown方法的第二个参数传false
                    boolean bool = moveDown(offsetY, false);
                    mLastY = (int) -distanceY;
                    return bool;
                } else {
                    return false;
                }
            } else {
                return false;
            }
        }
    };

    private static class AutoScroller implements Runnable {

        private final ScrollHelper mScroller;
        private int mLastY;
        private boolean mIsFinish;

        private EventHandler mEventHandler;
        private SwipeRefreshContainer mSwipeRefreshLayout;

        public AutoScroller(SwipeRefreshContainer swipeRefreshLayout) {
            mSwipeRefreshLayout = swipeRefreshLayout;
            mScroller = new ScrollHelper();
            mEventHandler = new EventHandler(EventRunner.getMainEventRunner());
            mIsFinish = true;
        }

        @Override
        public void run() {
            while (!mScroller.isFinished()) {
                mScroller.updateScroll();
                mSwipeRefreshLayout.moveDown(mLastY - mScroller.getCurrValue(ScrollHelper.AXIS_Y), true);
                mLastY = mScroller.getCurrValue(ScrollHelper.AXIS_Y);
            }

            mEventHandler.removeTask(this);
            mIsFinish = true;

            /*if (!mScroller.isFinished()) {
                boolean b = mScroller.updateScroll();
                System.out.println("updateScroll: " + b);
//                mScroller.updateScroll();
                mSwipeRefreshLayout.moveDown(mLastY - mScroller.getCurrValue(ScrollHelper.AXIS_Y), true);
                mLastY = mScroller.getCurrValue(ScrollHelper.AXIS_Y);
                mEventHandler.postTask(this);
            } else {
                mEventHandler.removeTask(this);
                mIsFinish = true;
            }*/
        }

        void recover(int dis) {
            mEventHandler.removeTask(this);
            mLastY = 0;
            mIsFinish = false;
            mScroller.startScroll(0, 0, 0, dis);
            mEventHandler.postTask(this);
        }

        boolean isFinish() {
            return mIsFinish;
        }
    }

}
