package com.talkweb.library.adapter.base.listener;

import com.talkweb.library.adapter.base.BaseViewHolder;

/**
 * Created by luoxw on 2016/6/20.
 */
public interface OnItemDragListener {
    void onItemDragStart(BaseViewHolder viewHolder, int pos);

    void onItemDragMoving(BaseViewHolder source, int from, BaseViewHolder target, int to);

    void onItemDragEnd(BaseViewHolder viewHolder, int pos);
}
